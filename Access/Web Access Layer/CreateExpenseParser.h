//
//  CreateExpenseParser.h
//  Access
//
//  Created by Radhey  Shyam on 08/12/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CreateExpenseParserDelegate <NSObject>

-(void)CreateExpenseSuccess;; ///// No need to pass any object as the no response comes in this case. CHeck for onlyt status code 200
-(void)CreateExpenseFail:(NSError *)error;

@end

@interface CreateExpenseParser : NSObject


@property (strong, nonatomic)id<CreateExpenseParserDelegate>delegate;

-(void)CreateExpenseForUSerId:(NSString *)userID forQuantity:(NSString *)Quantity forDescription:(NSString *)desc withImageBase64:(NSString *)base64Image andAmount:(NSString *)amount;

@end
