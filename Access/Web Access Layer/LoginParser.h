//
//  LoginParser.h
//  Access
//
//  Created by Radhey  Shyam on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoginParserDelegate <NSObject>

-(void)LoginSuccessWithDeatils:(NSDictionary *)dictDetails;

-(void)LoginFailedWithError:(NSError *)error;



@end

@interface LoginParser : NSObject

@property (strong, nonatomic)id<LoginParserDelegate>delegate;


-(void)LoginWithEmail:(NSString *)email password:(NSString *)password;


@end
