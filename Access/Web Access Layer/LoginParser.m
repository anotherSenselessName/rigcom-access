//
//  LoginParser.m
//  Access
//
//  Created by Radhey  Shyam on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "LoginParser.h"

@implementation LoginParser

@synthesize delegate;


-(void)LoginWithEmail:(NSString *)email password:(NSString *)password
{
    
    NSString *strurl = [NSString stringWithFormat:@"https://dev-rigcomaccess.cs5.force.com/Auth/services/apexrest/Authrization?Username=%@&Password=%@&OrganizatinoId=00DO00000006R9A", email,password];
    
    NSURL *url = [[NSURL alloc]initWithString:[strurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
//    NSMutableDictionary *dictdata = [[NSMutableDictionary alloc]init];
//  
//    [dictdata setObject:email forKey:@"userName"];
//
//    [dictdata setObject:password forKey:@"password"];
//
//    [dictdata setObject:@"" forKey:@"companyName"];
//
//    NSMutableData *myReturn = [[NSMutableData alloc] initWithCapacity:20];
//    
//    NSArray *formKeys = [dictdata allKeys];
//    
//    NSArray *formValues = [dictdata allValues];
//    
//    for (int i = 0; i < [formKeys count]; i++)
//    {
//        
//        if (i>0)
//        {
//            [myReturn appendData:[@"&" dataUsingEncoding:NSASCIIStringEncoding]];
//        }
//        
//        NSString *str = [NSString stringWithFormat:@"%@=%@",[formKeys objectAtIndex:i],[formValues objectAtIndex:i]];
//        [myReturn appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
//        
//    }
    
    NSMutableURLRequest *myRequest = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myContent = [NSString stringWithFormat:@"application/x-www-form-urlencoded"];
    
    [myRequest setValue:myContent forHTTPHeaderField:@"Content-type"];
    
    [myRequest setHTTPMethod:@"GET"];
    
  //  [myRequest setHTTPBody:myReturn];
    
    [NSURLConnection sendAsynchronousRequest:myRequest queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError==nil) {
            
            NSError *localError = nil; // https://dev-rigcomaccess.cs5.force.com/Auth/services/apexrest/Authrization?userName=user&password=123&companyName=1232345
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
            NSLog(@"Edit Profile Response: %@", dict);

            if(localError == nil)
            {
                [self.delegate performSelector:@selector(LoginSuccessWithDeatils:) withObject:dict];
            }
            else
            {
                [self.delegate performSelector:@selector(LoginFailedWithError:) withObject:localError];
            }
        }
        else
        {
            [self.delegate performSelector:@selector(LoginFailedWithError:) withObject:connectionError];
        }
        
    }];

}

@end
