//
//  getSavedPDFListparser.m
//  Access
//
//  Created by Radhey  Shyam on 22/01/16.
//  Copyright © 2016 Priya Kaushik. All rights reserved.
//

#import "getSavedPDFListparser.h"

@implementation getSavedPDFListparser

@synthesize delegate;


-(void)getTheListOfSavedPDFWithUserId:(NSString *)Userid
{
    
    
    NSString *strurl = [NSString stringWithFormat:@"https://dev-rigcomaccess.cs5.force.com/Auth/services/apexrest/Attachment?EmployeeId=%@",APP_DELEGATE.LoggedUserId];
    
    NSURL *url = [[NSURL alloc]initWithString:[strurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableDictionary *dictdata = [[NSMutableDictionary alloc]init];
    
    
    NSMutableData *myReturn = [[NSMutableData alloc] initWithCapacity:20];
 
     NSArray *formKeys = [dictdata allKeys];
     
     NSArray *formValues = [dictdata allValues];
     
     for (int i = 0; i < [formKeys count]; i++)
     {
     
     if (i>0)
     {
     [myReturn appendData:[@"&" dataUsingEncoding:NSASCIIStringEncoding]];
     }
     
     NSString *str = [NSString stringWithFormat:@"%@=%@",[formKeys objectAtIndex:i],[formValues objectAtIndex:i]];
     [myReturn appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
     
     }
    
    NSMutableURLRequest *myRequest = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myContent = [NSString stringWithFormat:@"application/raw"];
    
    [myRequest setValue:myContent forHTTPHeaderField:@"Content-type"];
    
    [myRequest setHTTPMethod:@"POST"];
    
    [myRequest setHTTPBody:myReturn];
    
    [NSURLConnection sendAsynchronousRequest:myRequest queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError==nil) {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            int responseStatusCode = [httpResponse statusCode];
            
            NSError *localError = nil;
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
            NSLog(@"Saved Pdfs List Response: %@", dict);
            
            if(responseStatusCode ==  200)
            {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                NSLog(@" Saved Pdfs List  Response: %@", dict);
                
                if(localError == nil)
                {
                    [self.delegate performSelector:@selector(SavedPdfListSuccess:) withObject:dict];
                }
                else
                {
                    [self.delegate performSelector:@selector(SavedPdfListFailed:) withObject:localError];
                }
            }
            else
            {
                [self.delegate performSelector:@selector(SavedPdfListFailed:) withObject:localError];
            }
        }
        else
        {
            [self.delegate performSelector:@selector(SavedPdfListFailed:) withObject:connectionError];
        }
    }];
}

@end