//
//  CreateExpenseParser.m
//  Access
//
//  Created by Radhey  Shyam on 08/12/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import "CreateExpenseParser.h"

@implementation CreateExpenseParser

@synthesize delegate;

-(void)CreateExpenseForUSerId:(NSString *)userID forQuantity:(NSString *)Quantity forDescription:(NSString *)desc withImageBase64:(NSString *)base64Image andAmount:(NSString *)amount
{
    NSString *strurl = [NSString stringWithFormat:@"https://dev-rigcomaccess.cs5.force.com/Auth/services/apexrest/JobExpenseData?Quantity=%@&Description=%@&userId=%@&imageName=%f.png&Actual_Price=%@",Quantity,desc,APP_DELEGATE.LoggedUserId,NSTimeIntervalSince1970,amount];
    
    NSURL *url = [[NSURL alloc]initWithString:[strurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableDictionary *dictdata = [[NSMutableDictionary alloc]init];
    
    [dictdata setObject:[NSString stringWithFormat:@"{\"ExpenseData\":[{\"Quantity\":\"%@\",\"Description\":\"%@\"}]}",Quantity,desc ] forKey:@"Data"];
    [dictdata setObject:base64Image forKey:@"imageBody"];
    [dictdata setObject:APP_DELEGATE.LoggedUserId forKey:@"UserId"];
    [dictdata setObject:[NSString stringWithFormat:@"%f.png",NSTimeIntervalSince1970] forKey:@"ImageName"];

    
    NSMutableData *myReturn = [[NSMutableData alloc] initWithCapacity:20];
    /*
    NSArray *formKeys = [dictdata allKeys];
    
    NSArray *formValues = [dictdata allValues];
    
    for (int i = 0; i < [formKeys count]; i++)
    {
        
        if (i>0)
        {
            [myReturn appendData:[@"&" dataUsingEncoding:NSASCIIStringEncoding]];
        }
        
        NSString *str = [NSString stringWithFormat:@"%@=%@",[formKeys objectAtIndex:i],[formValues objectAtIndex:i]];
        [myReturn appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    */
    
    [myReturn appendData:[base64Image dataUsingEncoding:NSUTF8StringEncoding]];

    NSMutableURLRequest *myRequest = [NSMutableURLRequest requestWithURL:url];
    
    NSString *myContent = [NSString stringWithFormat:@"application/raw"];
    
    [myRequest setValue:myContent forHTTPHeaderField:@"Content-type"];
    
    [myRequest setHTTPMethod:@"POST"];
    
    [myRequest setHTTPBody:myReturn];
    
    [NSURLConnection sendAsynchronousRequest:myRequest queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError==nil) {
            
            
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            int responseStatusCode = [httpResponse statusCode];
            
            NSError *localError = nil;

            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
            NSLog(@" Create Expense Response: %@", dict);
            
            if(responseStatusCode ==  200)
            {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                NSLog(@" Create Expense Response: %@", dict);
                
//                if(localError == nil)
                {
                    [self.delegate performSelector:@selector(CreateExpenseSuccess) withObject:nil];
                }
//                else
//                {
//                    [self.delegate performSelector:@selector(CreateExpenseFail:) withObject:localError];
//                }
 
            }
            else
            {
                [self.delegate performSelector:@selector(CreateExpenseFail:) withObject:localError];

            }
            
        }
        else
        {
            [self.delegate performSelector:@selector(CreateExpenseFail:) withObject:connectionError];
        }
        
    }];
    
  }

@end
