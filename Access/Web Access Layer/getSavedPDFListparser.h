//
//  getSavedPDFListparser.h
//  Access
//
//  Created by Radhey  Shyam on 22/01/16.
//  Copyright © 2016 Priya Kaushik. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol getSavedPDFListparserDelegate <NSObject>

-(void)SavedPdfListSuccess:(NSDictionary *)dictTemp;

-(void)SavedPdfListFailed:(NSError *)error;

@end

@interface getSavedPDFListparser : NSObject

@property (strong, nonatomic)id<getSavedPDFListparserDelegate>delegate;

-(void)getTheListOfSavedPDFWithUserId:(NSString *)Userid;

@end
