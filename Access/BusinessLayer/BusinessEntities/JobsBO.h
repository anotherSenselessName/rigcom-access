//
//  JobsBO.h
//  Access
//
//  Created by Sobharaj Mohapatra on 21/11/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobsBO : NSObject

@property (nonatomic, strong) NSString *jobID;
@property (nonatomic, strong) NSString *jobDesc;


@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *siteAddress;
@property (nonatomic, strong) NSString *jobPostalCode;
@property (nonatomic, strong) NSString *jobCity;
@property (nonatomic, strong) NSString *siteContact;
@property (nonatomic, strong) NSString *projectMnger;
@property (nonatomic, strong) NSString *hsSiteCode;
@property (nonatomic, strong) NSString *jobDate;
@property (nonatomic, strong) NSString *contactNumber;

@property (nonatomic, strong) NSString *siteInfo;

@end
