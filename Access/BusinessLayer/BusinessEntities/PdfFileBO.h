//
//  PdfFileBO.h
//  Access
//
//  Created by Sobharaj Mohapatra on 29/10/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PdfFileBO : NSObject

@property (nonatomic, strong) NSString *strFileName;
@property (nonatomic, assign) double     fileSize;
@property (nonatomic, strong) NSString *thumbnailImg;
@property (nonatomic, strong) NSString *tag;
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) NSString *strRev;
@property(nonatomic, strong)  NSString *serverFileId;
@property(nonatomic, strong)  NSString *hashDB;
@property(nonatomic, assign)  BOOL isfile;

@end
