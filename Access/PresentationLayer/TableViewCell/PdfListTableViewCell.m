//
//  PdfListTableViewCell.m
//  Access
//
//  Created by Sobharaj Mohapatra on 29/10/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import "PdfListTableViewCell.h"

@implementation PdfListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
