//
//  PdfListTableViewCell.h
//  Access
//
//  Created by Sobharaj Mohapatra on 29/10/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PdfListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPdf1;
@property (weak, nonatomic) IBOutlet UIButton    *btnTitle1;

@property (weak, nonatomic) IBOutlet UIButton    *btnTitle2;
@property (weak, nonatomic) IBOutlet UIImageView *imgPdf2;

@property (weak, nonatomic) IBOutlet UIImageView *imgPdf3;
@property (weak, nonatomic) IBOutlet UIButton    *btnTitle3;
@end
