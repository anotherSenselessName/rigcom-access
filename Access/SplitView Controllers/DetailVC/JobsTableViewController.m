//
//  JobsTableViewController.m
//  Access
//
//  Created by Priya Kaushik on 09/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "JobsTableViewController.h"

@interface JobsTableViewController ()
@property (nonatomic,strong) NSArray *headerArray;
@property (nonatomic,strong) NSDictionary *dataDictionary;
@property (nonatomic) BOOL isVisible;// to show visible header
@property (nonatomic,strong) JbsDetailViewController *detailVC;
@end

@implementation JobsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _headerArray = @[@"Proposals",@"Jobs",@"Employees"];
    
    _dataDictionary = @{@"Proposals":@[], @"Jobs":@[@"Site Forms",@"Inspection Forms",@"Installation Forms",@"MSDS's",@"Expenses"],@"Employees":@[]};
    // sets table footer
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [_headerArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (_isVisible) {// if any drop down is selected then we're showing number of rows in that section
        return [[_dataDictionary valueForKey:[_headerArray objectAtIndex:section]]count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
    NSArray *array = [_dataDictionary valueForKey:[_headerArray objectAtIndex:indexPath.section]];
    cell.textLabel.text = [array objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row!=4) {// all VC's are same except expenses
        [self performSegueWithIdentifier:@"FormSegue" sender:self];
    }
    else// show expenses VC
    {
         [self performSegueWithIdentifier:@"expensesSegue" sender:self];
    }
   
}

#pragma mark - UITableViewDelegate

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,50)];
    UIButton *headerButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0, CGRectGetWidth(headerView.frame), CGRectGetHeight(headerView.frame))];
    //sets title and background color
    [headerButton setTintColor:[UIColor whiteColor]];
    [headerButton setBackgroundColor:[UIColor colorWithRed:114/255.0f green:115/255.0f blue:118/255.0f alpha:1]];
    
    [headerButton setTitle:[_headerArray objectAtIndex:section] forState:UIControlStateNormal];
    
    //can select if header is jobs
    if (section == 1) {
        [headerButton addTarget:self action:@selector(headerSelected:) forControlEvents:UIControlEventTouchDown];
        
        if (_isVisible) {
            [headerButton setBackgroundImage:[UIImage imageNamed:@"stripImage"] forState:UIControlStateNormal];//set background color
        }
    }
    
    // make bottom black line
    UIView *bottomLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 48, tableView.frame.size.width, 2)];
    [bottomLineView setBackgroundColor:[UIColor blackColor]];
    
    [headerView addSubview:headerButton];
    [headerView addSubview:bottomLineView];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
-(IBAction)headerSelected:(id)sender
{
    _isVisible = _isVisible== YES ? NO :YES;
    [self performSegueWithIdentifier:@"JobsSegue" sender:self];// jobs Segue
    [self.tableView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"JobsSegue"]) {// do something with jobs segue
        
    }
    
    else if ([segue.identifier isEqualToString:@"FormSegue"])// segue to different forms
    {
    }
}

- (IBAction)homeScreen:(UIButton *)sender {
    
     [self performSegueWithIdentifier:@"JobsSegue" sender:self];
}
@end
