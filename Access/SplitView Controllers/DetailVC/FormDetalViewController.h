//
//  FormDetalViewController.h
//  Access
//
//  Created by Nishi on 20/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "getSavedPDFListparser.h"


@class DBRestClient;


@interface FormDetalViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,DBRestClientDelegate,getSavedPDFListparserDelegate>
{
    NSArray* photoPaths;
    NSString* photosHash;
    DBRestClient* restClient;
    NSString* currentPhotoPath;
    BOOL working;
    UIActivityIndicatorView* activityIndicator;
    
    NSMutableArray *arrData;
    
    NSMutableArray *arrSavedPdfs;

    NSMutableArray *arrSavedPdfsResponse;
    NSMutableArray *arrFolders;

    
    NSMutableArray *arrTempPdfData;

}
- (IBAction)addButton:(UIBarButtonItem *)sender;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, readonly) DBRestClient* restClient;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *viewToast;
- (void)getAllPdfFiles;

-(void)SavedPdfListSuccess:(NSDictionary *)dictTemp;

-(void)SavedPdfListFailed:(NSError *)error;

@end
