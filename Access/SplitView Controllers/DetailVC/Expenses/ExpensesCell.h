//
//  ExpensesCell.h
//  Access
//
//  Created by Nishi on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertDelegate ;

@interface ExpensesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextField *quantityTextField;
@property (strong, nonatomic) IBOutlet UITextField *amountTexField;

@property (weak, nonatomic) IBOutlet UITextView *txtViewEXpences;
@property (weak, nonatomic) IBOutlet UIButton *btnPhoto;

@property (nonatomic,strong) id<AlertDelegate>delegate;
@end

@protocol AlertDelegate <NSObject>
-(void)showAlertOnFrame:(id)sender;
@end