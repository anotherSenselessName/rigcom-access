//
//  ExpensesViewController.m
//  Access
//
//  Created by Nishi on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "ExpensesViewController.h"
#import "ExpensesCell.h"
#import "Constant.h"
#import "Base64.h"
@interface ExpensesViewController ()<UITextFieldDelegate>

@end

@implementation ExpensesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // tablefooter view
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, _tableView.bounds.size.width, 8.01f)];
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellid=@"ExpensesCell";
    
    ExpensesCell *cell=(ExpensesCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil)
    {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"ExpensesCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    
    cell.quantityTextField.borderStyle = UITextBorderStyleLine;
    cell.quantityTextField.layer.cornerRadius = 2.0;
    cell.quantityTextField.layer.borderWidth = 1;
    cell.quantityTextField.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:151.0/255.0 blue:95.0/255.0 alpha:1.0] CGColor];

    
    cell.amountTexField.borderStyle = UITextBorderStyleLine;
    cell.amountTexField.layer.cornerRadius = 2.0;
    cell.amountTexField.layer.borderWidth = 1;
    cell.amountTexField.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:151.0/255.0 blue:95.0/255.0 alpha:1.0] CGColor];

    cell.delegate = self;

if(imgPIc)
{
    [cell.btnPhoto setImage:imgPIc forState:UIControlStateNormal];
}
else{
    [cell.btnPhoto setImage:nil forState:UIControlStateNormal];

}
    
    [cell.btnPhoto addTarget:self action:@selector(btnPhotoClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self tableView:tableView didDeselectRowAtIndexPath:indexPath];
}
#pragma mark - Expenses Cell Delegate

- (void)cell:(ExpensesCell *)cell presentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:NULL];
}


- (IBAction)addButton:(UIBarButtonItem *)sender {
    
    NSLog(@"Hello");
}

- (IBAction)btnPlusClicked:(id)sender {
    [APP_DELEGATE showCustomLoaderInWidow:APP_DELEGATE.window];
    
    [self SendTheExpenseDetails];
}

- (BOOL)shouldAutorotate {
    return NO;
}

#pragma mark - Expenses Cell delegate
-(void)btnPhotoClicked:(UIButton *)btnPhoto
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""
                                                                             message:@""
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
       UIAlertAction *galleryOption = [UIAlertAction actionWithTitle:@"From Library"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action)
                                    {
                                        [self takeImageOrSelectFromGallery:UIImagePickerControllerSourceTypePhotoLibrary];
                                    }];
    
    UIAlertAction *cameraOption = [UIAlertAction actionWithTitle:@"From Camera"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action)
                                    {
                                        [self takeImageOrSelectFromGallery:UIImagePickerControllerSourceTypeCamera];
                                    }];
    
    [alertController addAction:cameraOption];
    [alertController addAction:galleryOption];
    [alertController addAction:cancelAction];
    
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alertController
                                                     popoverPresentationController];
    popPresenter.sourceView = self.view;
    CGRect rects =  CGRectMake(btnPhoto.frame.origin.x, btnPhoto.frame.origin.y+100, btnPhoto.frame.size.width, btnPhoto.frame.size.height);
    
    popPresenter.sourceRect = rects;
    [alertController.view setTintColor:[UIColor redColor]];
    // iOS8
    [alertController.view setTintColor:ORANGE_COLOR];
    [self presentViewController:alertController animated:YES completion:nil];
    //iOS9
    [alertController.view setTintColor:ORANGE_COLOR];

}

#pragma mark - UIImagePickerDelegate

-(void)takeImageOrSelectFromGallery:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        picker.sourceType = sourceType;
      [self presentViewController:picker animated:YES completion:nil];
    }
    else
       [ [[UIAlertView alloc] initWithTitle:@"error" message:@"This is not available on device. Please select another source type" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
//     do something with this chosen image
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    UIImage  *img = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    if(!img)
    {
        
       img = [info valueForKey:UIImagePickerControllerEditedImage]; 
    }
    imgPIc = img;
    
    [_tableView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark-
#pragma mark- Crate Expense Parser Delegates
#pragma mark-

-(void)show_AlertView_OK:(NSString *)alert_Title message:(NSString *)display_Message tag:(int)alert_Tag {
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:alert_Title message:display_Message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag=alert_Tag;
    [alert show];
    alert=nil;
    
}



-(void)SendTheExpenseDetails
{
    UITextField *txtFldQnt = (UITextField *)[_tableView viewWithTag:100];
    UITextView *txtViewDesc = (UITextView *)[_tableView viewWithTag:101];
    UITextField *txtFldAmnt = (UITextField *)[_tableView viewWithTag:102];
    
    if(txtFldQnt.text.length==0)
    {
        [self show_AlertView_OK:@"" message:@"Please enter quantity." tag:100];
        return;
    }
    else if(txtFldAmnt.text.length==0)
    {
        [self show_AlertView_OK:@"" message:@"Please enter amount." tag:100];
                return;

    }
    else if([txtViewDesc.text isEqualToString:@"- - None - - "])
    {
        [self show_AlertView_OK:@"" message:@"Please enter description." tag:100];
            return;
        
    }
    else if(!imgPIc)
    {
        [self show_AlertView_OK:@"" message:@"Please select an image." tag:100];
        return;
        
    }

    NSString *strQntity = txtFldQnt.text;
    NSString *strDesc = txtViewDesc.text;
    NSString *strAmnt = txtFldAmnt.text;

    NSString *strImg= @"";
   
    NSData *data1 = UIImageJPEGRepresentation(imgPIc, 1.0f);
    strImg = [Base64 encode:data1];
    
    CreateExpenseParser *parser = [[ CreateExpenseParser alloc]init];
    parser.delegate = self;
    [parser CreateExpenseForUSerId:APP_DELEGATE.LoggedUserId forQuantity:strQntity forDescription:strDesc withImageBase64:strImg andAmount:strAmnt];
    
    parser =  nil;
    
}

-(NSString *)imageToNSString:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}


-(void)CreateExpenseSuccess
{
    [APP_DELEGATE removeCustomLoaderInWidow:APP_DELEGATE.window];
    [self performSelectorOnMainThread:@selector(CreateExpenseSuccessOnMainThread) withObject:nil waitUntilDone:NO];
    
}

-(void)CreateExpenseFail:(NSError *)error
{
     [APP_DELEGATE removeCustomLoaderInWidow:APP_DELEGATE.window];
    
    [self performSelectorOnMainThread:@selector(CreateExpenseFailOnMainThread:) withObject:error waitUntilDone:NO];
    
}


-(void)refresTblData
{
    UITextField *txtFldQnt = (UITextField *)[_tableView viewWithTag:100];
    UITextView *txtViewDesc = (UITextView *)[_tableView viewWithTag:101];
    UITextField *txtFldAmnt = (UITextField *)[_tableView viewWithTag:102];
    
    
    txtFldQnt.text = @"";
    txtViewDesc.text = @"- - None - - ";
    txtFldAmnt.text = @"";
    imgPIc = nil;
    
    [_tableView reloadData];

}

-(void)CreateExpenseSuccessOnMainThread
{
    [self refresTblData];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Your details have been successfully sent to us." message:@"Thank you!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
    alert =  nil;
    
}

-(void)CreateExpenseFailOnMainThread:(NSError *)error
{
    [self refresTblData];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning !" message:@"Some error occured while updating your expense. Please try after some time." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
    alert =  nil;
  
}


@end
