//
//  ExpensesViewController.h
//  Access
//
//  Created by Nishi on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpensesCell.h"
#import "CreateExpenseParser.h"


@interface ExpensesViewController : UIViewController<AlertDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,CreateExpenseParserDelegate>
{
    UIImage *imgPIc ;
}
- (IBAction)addButton:(UIBarButtonItem *)sender;
- (IBAction)btnPlusClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

////

-(void)CreateExpenseSuccess;; ///// No need to pass any object as the no response comes in this case. CHeck for onlyt status code 200
-(void)CreateExpenseFail:(NSError *)error;

@end
