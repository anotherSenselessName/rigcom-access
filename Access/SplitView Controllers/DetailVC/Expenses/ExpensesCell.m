//
//  ExpensesCell.m
//  Access
//
//  Created by Nishi on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "ExpensesCell.h"

@implementation ExpensesCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)takePhoto:(UIButton *)sender {
    [_delegate showAlertOnFrame:sender];
}
@end
