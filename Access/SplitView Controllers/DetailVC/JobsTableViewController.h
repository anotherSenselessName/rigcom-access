//
//  JobsTableViewController.h
//  Access
//
//  Created by Priya Kaushik on 09/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JbsDetailViewController.h"
@interface JobsTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIButton *goToHomeScreen;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)homeScreen:(UIButton *)sender;
@end
