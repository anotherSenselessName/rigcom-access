//
//  JobDescriptionCell.h
//  Access
//
//  Created by Nishi on 20/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobDescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAccounntNAmeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSiteAddrsValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSiteAddressValue3;
@property (weak, nonatomic) IBOutlet UIButton *btnShowMap;
@property (weak, nonatomic) IBOutlet UILabel *lblSiteContactValue;
@property (weak, nonatomic) IBOutlet UILabel *lblHSSiteCode;
@property (weak, nonatomic) IBOutlet UILabel *lblJObDate;
@property (weak, nonatomic) IBOutlet UILabel *lblCntctNo1;
@property (weak, nonatomic) IBOutlet UILabel *lblCntctNo2;
@property (weak, nonatomic) IBOutlet UILabel *lblProjMngValue;
@end
