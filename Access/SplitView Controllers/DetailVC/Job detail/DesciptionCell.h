//
//  DesciptionCell.h
//  Access
//
//  Created by Nishi on 20/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DesciptionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
