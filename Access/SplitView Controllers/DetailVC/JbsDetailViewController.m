//
//  JbsDetailViewController.m
//  Access
//
//  Created by Priya Kaushik on 09/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "JbsDetailViewController.h"
#import "JobsDetailCell.h"
#import "Utilities.h"
#import "JobsBO.h"
#import "JobsViewController.h"

@interface JbsDetailViewController ()


@end

@implementation JbsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // sets table footer
//    _jobsTableView.tableFooterView = [[UIView alloc] init];
    _jobsTableView.backgroundColor =  [UIColor clearColor];
    _jobsTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, _jobsTableView.bounds.size.width, 8.01f)];

    arrSearched = [[NSMutableArray alloc] init];

    _arrJobs = [[NSMutableArray alloc] init];
    
    if(APP_DELEGATE.arrJOBS.count)
    {
        [_arrJobs addObjectsFromArray:APP_DELEGATE.arrJOBS];
        [_jobsTableView reloadData];
    }
    else{
        _lblLOading.hidden = NO;
    [self getAllJobsFromService];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 134;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    if(isSearching)
    {
        return arrSearched.count;
        
    }
    else{
        return _arrJobs.count;
        
    }

    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"JobsDetailCell";
    
    JobsDetailCell *cell=(JobsDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell==nil) {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"JobsDetailCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }

    JobsBO *job;
    
    if(isSearching)
    {
        // Checking the index before use
        if(arrSearched.count==0)
        {
            return cell;
        }
        job =[arrSearched objectAtIndex:indexPath.section];
        
    }
    else
    {
        if(_arrJobs.count==0)
        {
            return cell;
        }
        
        job =[_arrJobs objectAtIndex:indexPath.section];
        
    }

    
    
    
    cell.jobName.text = job.jobID;
    
    cell.accountName.text=job.accountName;
    cell.jobDescription.text =job.jobDesc;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    JobsBO *job =nil;
    if(isSearching)
    {
        job =[arrSearched objectAtIndex:indexPath.section];
        
    }
    else{
        job =[_arrJobs objectAtIndex:indexPath.section];
        
    }
    

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    JobsViewController *jobsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"JobsViewController"];
    jobsViewController.jobsBO = job;
    [self.navigationController pushViewController:jobsViewController animated:YES];
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

#pragma mark - UIToolBar Delegate
- (IBAction)addButton:(UIBarButtonItem *)sender {
    
    
}
- (IBAction)downButton:(id)sender {
    
    _jobsTableView.hidden = NO;
}

- (IBAction)btnLogoutClicked:(id)sender {
    
    [APP_DELEGATE.arrPdfFile removeAllObjects];

    [[DBSession sharedSession] unlinkAll];
//    [[DBSession sharedSession] unlinkUserId:APP_DELEGATE.relinkUserId];
    
    
//    [[NSURLCache sharedURLCache] removeCachedResponseForRequest:NSURLRequest];
  //  This would remove a cached response for a specific request. There is also a call that will remove all cached responses for all requests ran on the UIWebView:
        
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
  //  After that, you can try deleting any associated cookies with the UIWebView:
    
    for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        
        if([[cookie domain] isEqualToString:@"dropbox.com"]) {
            
            NSLog(@"hello Dropbox coockie is deleted");
            
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - Service methods
-(void)getAllJobsFromService
{
    
    [APP_DELEGATE networkRequestStarted];
    

    /////
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"https://dev-rigcomaccess.cs5.force.com/Auth/services/apexrest/Expenses/"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {

    
    if (connectionError==nil)
        {
            NSError *localError = nil;
            
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
            
            if(localError== nil)
            {
                NSMutableArray *arrData = [[NSMutableArray alloc] init];
                
                NSArray *aarResult = [parsedObject valueForKey:@"ExpenseData"];
                
                
                for (NSDictionary *dict in aarResult) {
                    
                    JobsBO *job = [[JobsBO alloc] init];
                    
                    
                    id dictname = [dict valueForKey:@"name"];
                    if (![dictname isKindOfClass:[NSNull class]])
                    {
                        job.jobID =[dict valueForKey:@"name"];
                    }
                    
                    id dictaccount_name__c = [dict valueForKey:@"account_name__c"];
                    if (![dictaccount_name__c isKindOfClass:[NSNull class]])
                    {
                        job.accountName =[dict valueForKey:@"account_name__c"];
                    }

                    
                    id dictjob_description__c = [dict valueForKey:@"job_description__c"];
                    if (![dictjob_description__c isKindOfClass:[NSNull class]])
                    {
                        job.jobDesc =[dict valueForKey:@"job_description__c"];
                    
                    }
                    
                    id dictjob_street__c = [dict valueForKey:@"job_street__c"];
                    if (![dictjob_street__c isKindOfClass:[NSNull class]])
                    {
                        job.siteAddress =[dict valueForKey:@"job_street__c"];
                    }
                    
                    id dictjob_city__c = [dict valueForKey:@"job_city__c"];
                    if (![dictjob_city__c isKindOfClass:[NSNull class]])
                    {
                        job.jobCity =[dict valueForKey:@"job_city__c"];
                    }

                    id dictjob_zip_postal_code__c = [dict valueForKey:@"job_zip_postal_code__c"];
                    if (![dictjob_zip_postal_code__c isKindOfClass:[NSNull class]])
                    {
                        job.jobPostalCode =[dict valueForKey:@"job_zip_postal_code__c"];
                    }


                    id dicths_site_code__c = [dict valueForKey:@"hs_site_code__c"];
                    if (![dicths_site_code__c isKindOfClass:[NSNull class]])
                    {
                        job.hsSiteCode =[dict valueForKey:@"hs_site_code__c"];
                    }

                    id dictjob_start_date__c = [dict valueForKey:@"job_start_date__c"];
                    if (![dictjob_start_date__c isKindOfClass:[NSNull class]])
                    {
                        job.jobDate =[dict valueForKey:@"job_start_date__c"];
                    }
                    

                    
                    [arrData addObject:job];
                    job = nil;
                    
                    
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(arrData.count)
                    {
                        [_arrJobs removeAllObjects];
                        [_arrJobs addObjectsFromArray:arrData];
                        
                        [APP_DELEGATE.arrJOBS removeAllObjects];
                        [APP_DELEGATE.arrJOBS addObjectsFromArray:arrData];
                    }
                    [_jobsTableView reloadData];
                    
                    [APP_DELEGATE networkRequestStopped];
                    _lblLOading.hidden = YES;

                });
                
            }
            else
            {
                [APP_DELEGATE networkRequestStopped];
                _lblLOading.hidden = YES;

            }
        }
        else
        {
            [APP_DELEGATE networkRequestStopped];
            _lblLOading.hidden = YES;

        }
    }];
}
#pragma mark - SearchBar Delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self tableviewExtedFrame];
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar // called when text starts editing
{
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar // called when text ends editing
{
    [self tableViewActualFrame];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText// called when text changes (including clear)
{
    isSearching = YES;
    
    if ([searchText length]) {
        [self filtercityData:searchText];
    }
    else {
        [arrSearched removeAllObjects];
        [arrSearched addObjectsFromArray:_arrJobs];
    }
    
    
    [_jobsTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1// called when keyboard search button pressed
{
    [self tableViewActualFrame];
    
    //      = NO;
    [_searchBar resignFirstResponder];
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar1
{
    [self tableViewActualFrame];
    [_searchBar resignFirstResponder];
    _searchBar.text = nil;
    isSearching = NO;
    [_jobsTableView reloadData];
}

-(void)filtercityData:(NSString *)filterParameter
{
    
    NSString *strTemp = [NSString stringWithFormat: @"%@",filterParameter];
    NSString *strTemp1=nil;
    
    if ([_arrJobs count]) {
        // To check which type of objects, that main array contained.
        strTemp1 = [NSString stringWithFormat:@"%@ CONTAINS[cd] '%@'",@"jobID", strTemp];
        [arrSearched removeAllObjects];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:strTemp1];
        [arrSearched addObjectsFromArray:[_arrJobs filteredArrayUsingPredicate:predicate]];
        [_jobsTableView reloadData];
        
    }
    
}

-(void)tableViewActualFrame
{
//    _jobsTableView.backgroundColor = [UIColor blueColor];
    //
    //    _tblViewTopSpace.constant = 41;
    //    _searchBarTopSpace.constant = 0;
        _tblVieBtmspace.constant = 0;
    //
    
}
-(void)tableviewExtedFrame
{
    //
    //    _tblViewTopSpace.constant = -_mapView.frame.size.height+44;
    //    _searchBarTopSpace.constant = -_mapView.frame.size.height;
        _tblVieBtmspace.constant = 500-40;
    //
    //    
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return  YES;
}// return NO to not resign first responder
@end
