//
//  JbsDetailViewController.h
//  Access
//
//  Created by Priya Kaushik on 09/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JbsDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSMutableArray *arrSearched;
    BOOL isSearching;

}

- (IBAction)addButton:(UIBarButtonItem *)sender;
- (IBAction)downButton:(id)sender;
- (IBAction)btnLogoutClicked:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblVieBtmspace;
@property (weak, nonatomic) IBOutlet UILabel *lblLOading;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *jobsTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *downButon;
@property (nonatomic, strong) NSMutableArray *arrJobs;
@end
