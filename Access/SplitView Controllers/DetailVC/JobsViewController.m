//
//  JobsViewController.m
//  Access
//
//  Created by Nishi on 20/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "JobsViewController.h"
#import "DesciptionCell.h"
#import "JobDescriptionCell.h"


@interface JobsViewController ()

@end

@implementation JobsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // sets table footer
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            
            static NSString *cellid=@"JobsDetailCell";
            
            JobDescriptionCell *cell=(JobDescriptionCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"JobsDetailCell" owner:self options:nil];
                cell=[nib objectAtIndex:0];
            }
            
            
            cell.btnShowMap.tag = indexPath.section + 500;
            
            [cell.btnShowMap addTarget:self action:@selector(btnShowMapClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.lblAccounntNAmeValue.text = _jobsBO.accountName.length?_jobsBO.accountName:@"";
            cell.lblSiteAddrsValue.text = _jobsBO.siteAddress.length?_jobsBO.siteAddress:@"";
            
            NSString *strAdd = @"";
            if(_jobsBO.jobCity.length)
            {
                strAdd = _jobsBO.jobCity;
                strAdd = [NSString stringWithFormat:@"%@,%@", strAdd,_jobsBO.jobPostalCode.length?_jobsBO.jobPostalCode:@""];
            }
            else{
                strAdd = _jobsBO.jobPostalCode.length?_jobsBO.jobPostalCode:@"";

            }
            
            cell.lblSiteAddressValue3.text =strAdd;
            cell.lblSiteContactValue.text = _jobsBO.siteContact.length?_jobsBO.siteContact:@"";
            cell.lblHSSiteCode.text = _jobsBO.hsSiteCode.length?_jobsBO.hsSiteCode:@"";
            cell.lblJObDate.text =_jobsBO.jobDate.length?_jobsBO.jobDate:@"";
            cell.lblCntctNo1.text = _jobsBO.contactNumber.length?_jobsBO.contactNumber:@"";
            cell.lblCntctNo2.text = _jobsBO.contactNumber.length?_jobsBO.contactNumber:@"";
            cell.lblProjMngValue.text =_jobsBO.projectMnger.length?_jobsBO.projectMnger:@"";
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            break;

            
        }
        case 1:
        {
            static NSString *cellid=@"JobsDecriptionCell";
            
            DesciptionCell *cell=(DesciptionCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"JobsDecriptionCell" owner:self options:nil];
                cell=[nib objectAtIndex:0];
            }
            cell.titleLabel.text = _jobsBO.jobDesc;
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            break;

        }
         case 2:
        {
            static NSString *cellid=@"SiteInformationCell";
            
            DesciptionCell *cell=(DesciptionCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"JobsDecriptionCell" owner:self options:nil];
                cell=[nib objectAtIndex:0];
            }
            cell.titleLabel.text = _jobsBO.siteInfo;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            break;

        }
    }
    return nil;
}


- (IBAction)addButton:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnShowMapClicked:(UIButton *)btnShowMap
{
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    JobMapViewController *jobMapViewController = [storyBoard instantiateViewControllerWithIdentifier:@"JobMapViewController"];
    jobMapViewController.callBack = self;
    
    _popoverController = [[UIPopoverController alloc] initWithContentViewController:jobMapViewController];
    
    CGRect rect = CGRectMake(60, 173, 128, 30);
    
    [_popoverController presentPopoverFromRect:rect
                                       inView:self.view
                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                     animated:YES];

}
-(void)dismissPopover
{
    [_popoverController dismissPopoverAnimated:TRUE];
}
@end
