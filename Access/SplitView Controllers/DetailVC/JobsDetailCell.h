//
//  JobsDetailCell.h
//  Access
//
//  Created by Nishi on 20/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobsDetailCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *jobName;
@property (strong, nonatomic) IBOutlet UILabel *accountName;
@property (strong, nonatomic) IBOutlet UILabel *jobDescription;

@property (strong, nonatomic)IBOutlet UIView *viewBack;

@end
