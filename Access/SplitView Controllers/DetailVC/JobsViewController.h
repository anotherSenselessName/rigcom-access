//
//  JobsViewController.h
//  Access
//
//  Created by Nishi on 20/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobsBO.h"
#import "JobMapViewController.h"

@interface JobsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,JobMapViewControllerDelegate>

@property (nonatomic, strong) JobsBO *jobsBO;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,retain) UIPopoverController *popoverController;

@end
