//
//  PdfDetailViewController.m
//  Access
//
//  Created by Sobharaj Mohapatra on 29/10/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import "PdfDetailViewController.h"

#import "Base64.h"

@interface PdfDetailViewController ()

@end

@implementation PdfDetailViewController

@synthesize IsFromCompletedForms;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];
    
    // Do any additional setup after loading the view.
    
    if(_pdfFileBO){
    _lblTitle.text = _pdfFileBO.strFileName;
    
        NSString *filePath = [self photoPath];

        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] == YES)
        {
       
            strRev =_pdfFileBO.strRev;

            [self loadPDFView];
        }
        else
        {
            [self.restClient loadFile:_pdfFileBO.filePath intoPath:[self photoPath]];
        }
    }
    
    if(IsFromCompletedForms)
    {
        
        self.btnSave.hidden =  YES;
    }
    
}
- (void)deviceOrientationDidChange:(NSNotification *)notification {
    //Obtaining the current device orientation
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    //Ignoring specific orientations
    if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationUnknown || currentOrientation == orientation) {
        return;
    }
//    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(relayoutLayers) object:nil];
    //Responding only to changes in landscape or portrait
    currentOrientation = orientation;
    //
    [self performSelector:@selector(orientationChangedMethod) withObject:nil afterDelay:0];
}

-(void)orientationChangedMethod
{
    if(currentOrientation == UIDeviceOrientationLandscapeLeft || currentOrientation == UIDeviceOrientationLandscapeRight)
    {
        
        // Create a new PDFViewCtrl that is the size of the entire screen
        pdfViewCtrl .frame  =CGRectMake(0, 128, APP_DELEGATE.window.frame.size.width-375, APP_DELEGATE.window.frame.size.height-128);

    }
    else
    {
        pdfViewCtrl .frame  =CGRectMake(0, 128, APP_DELEGATE.window.frame.size.width, APP_DELEGATE.window.frame.size.height-128);

    }
}

- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)destPath contentType:(NSString*)contentType metadata:(DBMetadata*)metadata
{

    strRev = metadata.rev;
    
    _pdfFileBO.strRev = strRev;

    PdfFileBO *pdf = [PdfFileBO new];
    pdf.strFileName = _pdfFileBO.strFileName;
    pdf.fileSize = _pdfFileBO.fileSize;
    pdf.thumbnailImg = _pdfFileBO.thumbnailImg;
    pdf.tag = _pdfFileBO.tag;
    pdf.filePath = _pdfFileBO.filePath;
    pdf.strRev = _pdfFileBO.strRev;
    
    if([APP_DELEGATE.arrPdfFile containsObject:_pdfFileBO])
    {
    [APP_DELEGATE.arrPdfFile replaceObjectAtIndex:[APP_DELEGATE.arrPdfFile indexOfObject:_pdfFileBO] withObject:pdf];
    }
    NSMutableArray *arrTemp = APP_DELEGATE.arrPdfFile ;
    
    NSLog(@"Success loaded at :%@ ",destPath);
    
//    NSString *filePath = [self photoPath];
//    
//    NSURL *url = [NSURL fileURLWithPath:filePath];
//    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    
//    [_webViewPdf loadRequest:request];
    
    
    [self loadPDFView];

}
- (void)restClient:(DBRestClient*)client loadProgress:(CGFloat)progress forFile:(NSString*)destPath
{
    NSLog(@"progress:%f",progress);
}
- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error
{
    NSLog(@"loadFileFailedWithError");
}


-(void)saveFileToServer
{
    [self.restClient uploadFile:_pdfFileBO.strFileName toPath:_pdfFileBO.filePath withParentRev:strRev fromPath:[self photoPath]];
}

- (NSString*)photoPath {
    
    NSString *strName = [_pdfFileBO.strFileName stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [NSTemporaryDirectory() stringByAppendingPathComponent:strName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackClicked:(id)sender {
    
    if(!IsFromCompletedForms)
    [self saveDocument];
    [self.navigationController popViewControllerAnimated:YES];
}


- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

-(void)loadPDFView
{
    [PTPDFNet Initialize:@""];
    
    NSString *filePath = [self photoPath];

    // Register the resource file
    NSString* resourcePath = [[NSBundle mainBundle] pathForResource:@"pdfnet" ofType:@"res"];
    [PTPDFNet SetResourcesPath:resourcePath];
    
    // Get the path to document in the app bundle.
    NSString* fullPath = filePath;
    
    // Initialize a new PDFDoc with the path to the file
    PTPDFDoc* docToOpen = [[PTPDFDoc alloc] initWithFilepath:fullPath];
    
    // Create a new PDFViewCtrl that is the size of the entire screen
     pdfViewCtrl = [[PTPDFViewCtrl alloc] initWithFrame:CGRectMake(0, 128, self.view.frame.size.width-375, self.view.frame.size.height-128)];
    
    
    NSLog(@"pdfViewCtrl Desv: %@",pdfViewCtrl.description);
    // Set the document to display
    [pdfViewCtrl SetDoc:docToOpen];
    
    

    // Add the PDFViewCtrl to the root view
    [self.view addSubview:pdfViewCtrl];

        _document = docToOpen;
    
    // set the background of the view to light gray
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // set the background of PDFViewCtrl to transparent (alpha is zero.)
//    [pdfViewCtrl SetBackgroundColor:255 g:255 b:255 a:0];
    
    // creates a new tool manager using the designated initializer
    ToolManager* toolManager = [[ToolManager alloc] initWithPDFViewCtrl:pdfViewCtrl];
    
    // registers the tool manager to receive events
    [pdfViewCtrl setToolDelegate:toolManager];
    
    // sets the initial tool
    [toolManager changeTool:[PanTool class]];
    
    [self deviceOrientationDidChange:nil];
 
}
- (void)saveDocument
{
    bool isRendering = false;
    isRendering = ![pdfViewCtrl IsFinishedRendering:NO];
    if( isRendering )
        [pdfViewCtrl CancelRendering];
    
    [pdfViewCtrl DocLock:YES];
    
    if ([self photoPath] && self.document && [self.document IsModified]) {
        @try {
            {
                [self.document SaveToFile:[self photoPath] flags:e_ptincremental];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@", exception);
        }
    }
    
    [pdfViewCtrl DocUnlock];
}

- (IBAction)btnSaveClicked:(id)sender {

    [self saveDocument];
    [self performSelectorInBackground:@selector(SaveToSalesForce) withObject:nil];
    

}

- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath from:(NSString*)srcPath
          metadata:(DBMetadata*)metadata
{
    
}
- (void)restClient:(DBRestClient*)client uploadProgress:(CGFloat)progress
           forFile:(NSString*)destPath from:(NSString*)srcPath
{
    
}
- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error
{
    
}

#pragma mark
#pragma mark Send to Sales force
#pragma mark

-(void)SaveToSalesForce
{
    
    NSString *strurl =[NSString stringWithFormat: @"https://dev-rigcomaccess.cs5.force.com/Auth/services/apexrest/JobExpensePdfCreate?EmployeeID=%@&FileName=%@",APP_DELEGATE.LoggedUserId, _pdfFileBO.strFileName];
    
    NSURL *url = [[NSURL alloc]initWithString:[strurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableDictionary *dictdata = [[NSMutableDictionary alloc]init];
    
    NSString *strEncoded = [Base64 encode:[NSData dataWithContentsOfFile:[self photoPath]]];

     [dictdata setObject:[NSString stringWithFormat:@"{\"pdf_name\":\"%@\",\"base64\":\"%@\"}",_pdfFileBO.strFileName,strEncoded ] forKey:@"Data"];
    
    NSMutableData *myReturn = [[NSMutableData alloc] initWithCapacity:20];
  
    /*
     
    NSArray *formKeys = [dictdata allKeys];
    
    NSArray *formValues = [dictdata allValues];
    
    for (int i = 0; i < [formKeys count]; i++)
    {
        
        if (i>0)
        {
            [myReturn appendData:[@"&" dataUsingEncoding:NSASCIIStringEncoding]];
        }
        
        NSString *str = [NSString stringWithFormat:@"%@=%@",[formKeys objectAtIndex:i],[formValues objectAtIndex:i]];
        [myReturn appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    */
    
    
    
    [myReturn appendData:[strEncoded dataUsingEncoding:NSUTF8StringEncoding]];

    
    NSMutableURLRequest *myRequest = [NSMutableURLRequest requestWithURL:url];
    
//    NSString *myContent = [NSString stringWithFormat:@"application/x-www-form-urlencoded"];
    NSString *myContent = [NSString stringWithFormat:@"application/raw"];
  
    [myRequest setValue:myContent forHTTPHeaderField:@"Content-type"];
    
    [myRequest setHTTPMethod:@"POST"];
    
    [myRequest setHTTPBody:myReturn];
    
    [NSURLConnection sendAsynchronousRequest:myRequest queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (connectionError==nil) {
            
            
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            int responseStatusCode = [httpResponse statusCode];
            
            NSError *localError = nil;
            
            
            if(responseStatusCode ==  200)
            {
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                NSLog(@" Create Expense Response: %@", dict);
                
                //                if(localError == nil)
                {
                    [self performSelectorOnMainThread:@selector(SaveToSalesForceSuccess:) withObject:dict waitUntilDone:NO];
                }
                //                else
                //                {
                //                    [self.delegate performSelector:@selector(CreateExpenseFail:) withObject:localError];
                //                }
                
            }
            else
            {
                [self performSelectorOnMainThread:@selector(SaveToSalesForceFailed:) withObject:localError waitUntilDone:NO];
            }
        }
        else
        {
            [self performSelectorOnMainThread:@selector(SaveToSalesForceFailed:) withObject:connectionError waitUntilDone:NO];
        }
        
    }];
    
}

-(void)SaveToSalesForceSuccess:(NSDictionary *)dictDeatils
{
    
    UIAlertView *alert  = [[ UIAlertView alloc ]initWithTitle:@"Your file have been uploaded to Dropbox and Salesforce successfuly. " message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
    alert =  nil;
    
    
    
    [self.restClient uploadFile:@"" toPath:_pdfFileBO.filePath withParentRev:strRev fromPath:[self photoPath]];

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)SaveToSalesForceFailed:(NSError *)error
{
    [self performSelectorInBackground:@selector(SaveToSalesForce) withObject:nil];

    
}


@end
