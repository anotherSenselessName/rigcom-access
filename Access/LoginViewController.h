//
//  LoginViewController.h
//  
//
//  Created by Priya Kaushik on 09/09/15.
//
//

#import <UIKit/UIKit.h>

#import "LoginParser.h"

@interface LoginViewController : UIViewController<LoginParserDelegate>
@property (weak, nonatomic) IBOutlet UIView *vWlogin;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)login:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLayoutvWlogIn;


-(void)LoginSuccessWithDeatils:(NSDictionary *)dictDetails;

-(void)LoginFailedWithError:(NSError *)error;



@end
