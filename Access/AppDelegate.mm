//
//  AppDelegate.m
//  Access
//
//  Created by Priya Kaushik on 09/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import "AppDelegate.h"
#define tagForCustomLoader          9100

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize LoggedUserId;
@synthesize relinkUserId;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    _arrJOBS = [[NSMutableArray alloc] init];
    
    self.arrPdfFile= [[NSMutableArray alloc] init];
    NSString* appKey = @"hjyifyelbjevnoi";
    NSString* appSecret = @"horlda1xzw9x3tz";
    NSString *root = kDBRootDropbox; // Should be set to either kDBRootAppFolder or kDBRootDropbox
    // You can determine if you have App folder access or Full Dropbox along with your consumer key/secret
    // from https://dropbox.com/developers/apps
    
    // Look below where the DBSession is created to understand how to use DBSession in your app
    
    NSString* errorMsg = nil;
    if ([appKey rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        errorMsg = @"Make sure you set the app key correctly in DBRouletteAppDelegate.m";
    } else if ([appSecret rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        errorMsg = @"Make sure you set the app secret correctly in DBRouletteAppDelegate.m";
    } else if ([root length] == 0) {
        errorMsg = @"Set your root to use either App Folder of full Dropbox";
    } else {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSData *plistData = [NSData dataWithContentsOfFile:plistPath];
        NSDictionary *loadedPlist =
        [NSPropertyListSerialization
         propertyListFromData:plistData mutabilityOption:0 format:NULL errorDescription:NULL];
        NSString *scheme = [[[[loadedPlist objectForKey:@"CFBundleURLTypes"] objectAtIndex:0] objectForKey:@"CFBundleURLSchemes"] objectAtIndex:0];
        if ([scheme isEqual:@"db-APP_KEY"]) {
            errorMsg = @"Set your URL scheme correctly in DBRoulette-Info.plist";
        }
    }
    
    DBSession* session =
    [[DBSession alloc] initWithAppKey:appKey appSecret:appSecret root:root];
    session.delegate = self; // DBSessionDelegate methods allow you to handle re-authenticating
    [DBSession setSharedSession:session];
    session = nil;
    
    [DBRequest setNetworkRequestDelegate:self];
    
    if (errorMsg != nil) {
        [[[UIAlertView alloc]
          initWithTitle:@"Error Configuring Session" message:errorMsg
          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]         show];
    }

    

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
        
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getAllPdfFiles" object:nil];
            
        }
        else
        {
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
                
                NSLog(@"%@",[cookie domain]);
                
                if([[cookie domain]rangeOfString: @"dropbox.com"].location!=NSNotFound) {
                    
                    NSLog(@"hello Dropbox coockie is deleted");
                    
                    [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
                }
            }
        }
        return YES;
    }
    
    return NO;
    
}

#pragma mark -
#pragma mark DBSessionDelegate methods

- (void)sessionDidReceiveAuthorizationFailure:(DBSession*)session userId:(NSString *)userId {
    relinkUserId = userId ;
    [[[UIAlertView alloc]
      initWithTitle:@"Dropbox Session Ended" message:@"Do you want to relink?" delegate:self
      cancelButtonTitle:@"Cancel" otherButtonTitles:@"Relink", nil]show];
}


#pragma mark -
#pragma mark UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)index {
    if (index != alertView.cancelButtonIndex)
    {
//[[DBSession sharedSession] linkFromController:];// - See more at: http://www.theappguruz.com/blog/ios-dropbox-integration#sthash.fGwjyrL7.dpuf
    }
        
    relinkUserId = nil;
}


#pragma mark -
#pragma mark DBNetworkRequestDelegate methods

static int outstandingRequests;

- (void)networkRequestStarted {
    outstandingRequests++;
    if (outstandingRequests == 1) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
}

- (void)networkRequestStopped {
    outstandingRequests--;
    if (outstandingRequests == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

-(void)showCustomLoader:(UIViewController *)senderController
{
    if( [senderController.view viewWithTag:tagForCustomLoader])
        [[senderController.view viewWithTag:tagForCustomLoader] removeFromSuperview];
    CustomLoader *custLoader=[[CustomLoader alloc]initWithFrame:self.window.frame];
    custLoader.tag=tagForCustomLoader;
    [custLoader loadView];
    [senderController.view addSubview:custLoader];
    custLoader=nil;
}
-(void)removeCustomLoader:(UIViewController *)senderController
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if([senderController.view viewWithTag:tagForCustomLoader])
                [[senderController.view viewWithTag:tagForCustomLoader] removeFromSuperview];
        });
    });
}

-(void)showCustomLoaderInWidow:(UIWindow *)senderController
{
    CustomLoader *custLoader=[[CustomLoader alloc]initWithFrame:self.window.frame];
    custLoader.tag=tagForCustomLoader;
    [custLoader loadView];
    [senderController addSubview:custLoader];
    custLoader=nil;
}
-(void)removeCustomLoaderInWidow:(UIWindow *)senderController
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if([senderController viewWithTag:tagForCustomLoader])
                [[senderController viewWithTag:tagForCustomLoader] removeFromSuperview];
        });
    });
}


@end
