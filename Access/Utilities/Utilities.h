//
//  Utilities.h
//  Access
//
//  Created by Nishi on 21/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utilities : NSObject
+(void)roundTableViewCell:(UITableViewCell *)cell InTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
@end
