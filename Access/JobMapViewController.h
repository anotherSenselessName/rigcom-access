//
//  JobMapViewController.h
//  Access
//
//  Created by Sobharaj Mohapatra on 21/11/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol JobMapViewControllerDelegate <NSObject>
@optional
-(void)dismissPopover;
@end


@interface JobMapViewController : UIViewController<CLLocationManagerDelegate>
{
    CLLocationCoordinate2D userCutterntlocation;
}
@property (nonatomic, retain) id <JobMapViewControllerDelegate>callBack;

@property (weak, nonatomic) IBOutlet MKMapView *jobMapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
- (IBAction)btnCloseClicked:(id)sender;

@end
