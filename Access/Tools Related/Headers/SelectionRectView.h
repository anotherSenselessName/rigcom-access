//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NoFadeTiledLayer.h"
#import <PDFNet/PDFViewCtrl.h>



@interface SelectionRectView : UIView {

    @public
    BOOL m_isLineAnnot;
    BOOL m_startAtNE;
    PTAnnot* m_annot;
    int m_rectOffset;
}

@property (nonatomic, weak) PTPDFViewCtrl* m_pdfViewCtrl;

-(id)initWithFrame:(CGRect)frame forAnnot:(PTAnnot*)annot;
-(void)setEditLine:(BOOL)isLine;
-(void)setAnnot:(PTAnnot*)annot;

@end
