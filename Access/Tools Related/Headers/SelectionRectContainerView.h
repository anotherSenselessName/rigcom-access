//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import "SelectionRectView.h"
#include "ResizeWidgetView.h"
#import <PDFNet/PDFViewCtrl.h>



@class SelectionRectView;


@interface SelectionRectContainerView : UIView {
    
    UITextView* tv;
    ResizeWidgetView* rwvNW;
    ResizeWidgetView* rwvN;
    ResizeWidgetView* rwvNE;
    ResizeWidgetView* rwvE;
    ResizeWidgetView* rwvSE;
    ResizeWidgetView* rwvS;
    ResizeWidgetView* rwvSW;
    ResizeWidgetView* rwvW;
    CGPoint startPoint, endPoint;
    
    @public
    SelectionRectView* selectionRectView;
}

@property (nonatomic, assign) BOOL isLineEdit;
@property (nonatomic, weak) PTPDFViewCtrl* m_pdfViewCtrl;

-(void)hideResizeWidgetViews;
-(void)showResizeWidgetViews;
-(void)setEditTextSizeForZoom:(double)zoom forFontSize:(int)size;
-(void)useTextViewWithText:(NSString*)text atZoom:(double)zoom forFontSize:(int)size withDelegate:(id<UITextViewDelegate>)delegateView;
-(void)setAnnotationContents:(PTAnnot*)annot;

-(void)showNESWWidgetViews;
-(void)showNWSEWidgetViews;
-(void)showLine;
-(void)hideLine;
-(void)setAnnot:(PTAnnot*)annot;

-(id)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl forAnnot:(PTAnnot*)annot;


@end
