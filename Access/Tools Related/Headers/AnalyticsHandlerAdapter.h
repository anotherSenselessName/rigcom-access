//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

#define ANALYTICS_LOG(__EXCEPTION, __EXTRADATA) [[AnalyticsHandlerAdapter getInstance] logException:__EXCEPTION withExtraData:__EXTRADATA];


@interface AnalyticsHandlerAdapter : NSObject

// Current instance of AnalyticsHandlerAdapter
+ (id) getInstance;
+ (void) setInstance:(AnalyticsHandlerAdapter*)value;

/**
 Sets a string for identifying the user.
 
 @param identifier the string identifying the user.
 */
- (void)setUserIdentifier:(NSString *)identifier;

/** 
 * Performs sdk specific initializations
 *
 */
- (void)initializeHandler;

- (BOOL) logException:(NSException *)exception withExtraData:(NSDictionary *)extraData;

- (BOOL) sendCustomEventWithTag:(NSString *)tag;

@end
