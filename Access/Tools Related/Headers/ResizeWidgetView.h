//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>


typedef enum {
    e_topleft,
    e_top,
    e_topright,
    e_right,
    e_bottomright,
    e_bottom,
    e_bottomleft,
    e_left
} Location;


@interface ResizeWidgetView : UIView {
    
    @public
    Location m_location;
}

- (id)initAtPoint:(CGPoint)point WithLocation:(Location)loc;

+(int)getLength;

@end
