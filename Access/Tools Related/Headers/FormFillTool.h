//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "Tool.h"

@interface FormFillTool : Tool<UIPopoverControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UITableViewDelegate>
{
    UIView* responder;
    BOOL keyboardOnScreen;
    NSMutableArray* choices;
    int characterLimit;
}

-(NSInteger)numberOfChoices;
-(NSString*)titleOfChoiceAtIndex:(NSUInteger)num;
-(NSMutableArray*)getSelectedItemsInActiveListbox;

@property (nonatomic) CGRect fontRect;

@end
