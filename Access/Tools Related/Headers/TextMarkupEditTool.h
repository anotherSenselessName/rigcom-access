//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "TextSelectTool.h"
#import "AnnotEditTool.h"

@interface TextMarkupEditTool : TextSelectTool

@property (nonatomic, assign) CGRect annotRect;

/**
 Used strictly as a utility class for text annotation editing.
 */
@property (nonatomic, retain) AnnotEditTool* annotEditUtilityTool;

-(void)selectCurrentTextMarkupAnnotation;

@end
