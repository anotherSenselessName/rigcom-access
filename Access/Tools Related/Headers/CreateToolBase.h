//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Tool.h"

@interface CreateToolBase : Tool {
    CGPoint startPoint;
    CGPoint endPoint;
    Class pdfNetAnnotType;
    CGRect drawArea;
    int pageNumber;
    NSUInteger m_num_touches;
	UIColor* _strokeColor;
	UIColor* _fillColor;
	double _opacity;
	double _thickness;
}

-(double)setupContextFromDefaultAnnot:(CGContextRef)currentContext;
-(void)setPropertiesFromDefaultAnnotation: (PTAnnot *) annotation;

@end
