//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import "AnnotEditTool.h"

@interface NoteEditController : UIViewController {
    Tool* delegate;

    @public
    UITextView* tv;
}

- (id)initWithDelegate:(Tool*)del;
-(void)cancelButtonPressed;
-(void)deleteButtonPressed;
-(void)saveButtonPressed;

@end
