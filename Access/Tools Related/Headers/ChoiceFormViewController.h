//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import "FormFillTool.h"

@interface ChoiceFormViewController: UIViewController<UITableViewDelegate, UITableViewDataSource>  {
    FormFillTool<UITableViewDelegate>* del;
}
@property (assign) BOOL isMultiSelect;

-(void)setDelegate:(FormFillTool<UITableViewDelegate>*)delegate;

@end
