//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CreateToolBase.h"

@interface Eraser : CreateToolBase {
    CGContextRef context;
    NSMutableArray* m_free_hand_points;
    double m_eraser_half_width;
    CGPoint m_current_point;
    CGPoint m_prev_point;
    NSMutableArray* m_ink_list;
}

-(void)commitAnnotation;
-(double)setupContextFromDefaultAnnot:(CGContextRef)currentContext;

@end
