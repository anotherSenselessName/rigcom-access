//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "CreateToolBase.h"

@interface StickyNoteCreate : CreateToolBase {
    UIImage* stickyIcon;
    BOOL creating;
	BOOL m_initialSticky;
}

@end
