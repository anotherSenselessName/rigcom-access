//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "Tool.h"
#import "UIKit/UIKit.h"
#import "MediaPlayer/MediaPlayer.h"

@interface RichMediaTool : Tool
{
	MPMoviePlayerController* moviePlayer;
	
	NSString* moviePath;
}

@end
