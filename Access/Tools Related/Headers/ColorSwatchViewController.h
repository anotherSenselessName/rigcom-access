//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import <PDFNet/PDFNetOBJC.h>


@class ColorSwatchViewController;

@protocol ColorPickerViewControllerDelegate <NSObject>
- (void)colorPickerViewController:(ColorSwatchViewController *)colorPicker didSelectColor:(UIColor *)color;
- (void)noteEditCancelButtonPressed;
@end

@interface ColorSwatchViewController : UIViewController
{
	BOOL m_forMarkup;
	BOOL m_forFreeText;
	BOOL m_forHighlight;
	BOOL m_forTextMarkup;
}

@property (nonatomic, assign) PTAnnotType annotationType;
@property (nonatomic, assign) BOOL forFillColor;

@property(nonatomic,weak)	id<ColorPickerViewControllerDelegate> delegate;

@end
