//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <PDFNet/PDFNetOBJC.h>


#define ATTRIBUTE_STROKE_COLOR		@"Color"
#define ATTRIBUTE_FILL_COLOR		@"FillColor"
#define ATTRIBUTE_OPACITY			@"Opacity"
#define ATTRIBUTE_BORDER_THICKNESS	@"BorderThickness"
#define ATTRIBUTE_FREETEXT_SIZE		@"FreeTextSize"

@interface ColorDefaults : NSObject

+(void)setDefaultColor:(UIColor*)color forAnnotType:(PTAnnotType)type attribute:(NSString*)attribute colorPostProcessMode:(PTColorPostProcessMode)mode;
+(UIColor*)defaultColorForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute colorPostProcessMode:(PTColorPostProcessMode)mode;

+(PTColorPt*)defaultColorPtForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute colorPostProcessMode:(PTColorPostProcessMode)mode;
+(int)numCompsInColorPtForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute;

+(void)setDefaultOpacity:(double)opacity forAnnotType:(PTAnnotType)type;
+(double)defaultOpacityForAnnotType:(PTAnnotType)type;

+(void)setDefaultBorderThickness:(double)thickness forAnnotType:(PTAnnotType)type;
+(double)defaultBorderThicknessForAnnotType:(PTAnnotType)type;

+(void)setDefaultFreeTextSize:(double)size forAnnotType:(PTAnnotType)type;
+(double)defaultFreeTextSizeForAnnotType:(PTAnnotType)type;

@end

