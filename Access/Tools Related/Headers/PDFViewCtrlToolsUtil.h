//
//  Util.h
//  Tools
//
//  Created by PDFTron on 2015-03-17.
//
//

#import <Foundation/Foundation.h>

@interface PDFViewCtrlToolsUtil : NSObject

+ (NSBundle*)toolsStringBundle;

@end
