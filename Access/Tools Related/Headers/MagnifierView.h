//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

@interface TrnMagnifierView : UIView {
	UIView *viewToMagnify;
	CGPoint magnifyPoint;
}

- (void)setMagnifyPoint:(CGPoint)mt TouchPoint:(CGPoint)tp;

@property (nonatomic, strong) UIView *viewToMagnify;
@property (assign) CGPoint magnifyPoint;

@end
