//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

@class DigSigView;
@protocol FloatingSigViewControllerDelegate;

@interface FloatingSigViewController : UIViewController
{
	DigSigView* m_digSigView;
	BOOL m_asDefault;
}

/**
 * An object that conforms to the DigSigViewControllerDelegate protocol.
 *
 */
@property (nonatomic, weak) id<FloatingSigViewControllerDelegate> delegate;

@end


@protocol FloatingSigViewControllerDelegate<NSObject>

@required
-(void)saveAppearanceWithPath:(NSMutableArray*)points withBoundingRect:(CGRect)boundingRect asDefault:(BOOL)asDefault;
-(void)closeSignatureDialog;

@end