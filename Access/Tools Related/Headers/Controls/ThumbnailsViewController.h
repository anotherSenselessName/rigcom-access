//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import <PDFNet/PDFViewCtrl.h>
#import "ThumbnailViewCell.h"
#import "UICollectionView+Draggable.h"

@interface ThumbnailsViewController : UIViewController<UICollectionViewDataSource_Draggable,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    UINavigationBar *_naviBar;
    UIToolbar *_editToolBar;
    UIBarButtonItem *_deleteButton;
    UIBarButtonItem *_selectAllButton;
    UIBarButtonItem *_selectedCountButton;
    
    PTPDFViewCtrl *_pdfViewCtrl;
    CGRect _thumbFrame;
    NSInteger _spacingSize;
    
    NSMutableDictionary *_thumbsList;
    NSMutableArray *_selectedList;
    
    int _currentPageIndex;
}

-(id)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl;
-(void)setThumbnail:(UIImage*)image forPage:(NSInteger)pageNum;

@end
