//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

@class FreehandCreateToolbar;

/**
 * The methods declared by the FreehandCreateToolbarDelegate protocol allow the adopting delegate to respond to messages from
 * the AnnotationToolbar class.
 *
 */
@protocol FreehandCreateToolbarDelegate <NSObject>

-(void)undoFreetextStroke:(UIButton*)button;
-(void)redoFreetextStroke:(UIButton*)button;
-(void)cancelFreehandToolbar:(UIButton*)button;
-(void)dismissFreehandToolbar:(UIButton*)button;

@end

/**
 * The FreehandCreateToolbar is used by the AnnotationToolbar to show an undo/redo interface while the user
 * is drawing freehand ink strokes.
 *
 */
@interface FreehandCreateToolbar : UIToolbar

/**
 * Enables the undo button.
 */
-(void)enableUndoButton;

/**
 * Enables the redo button.
 */
-(void)enableRedoButton;

/**
 * Disables the undo button.
 */
-(void)disableUndoButton;

/**
 * Enables the undo button.
 */
-(void)disableRedoButton;

@end
