//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <PDFNet/PDFNetOBJC.h>

@interface BookmarkUtils : NSObject

+(void)fileMovedFrom:(NSURL*)oldLocation to:(NSURL*)newLocation;
+(void)saveBookmarkData:(NSArray*)bookmarkData forFileUrl:(NSURL*)url;
+(NSArray*)bookmarkDataForDocument:(NSURL*)documentUrl;
+(NSMutableArray*)updateUserBookmarks:(NSMutableArray*) bookmarks oldPageNumber:(const unsigned int)oldPageNumber newPageNumber:(const unsigned int)newPageNumber oldSDFNumber:(const unsigned int)oldSDFNumber newSDFNumber:(const unsigned int)newSDFNumber;

@end

