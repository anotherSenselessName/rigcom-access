//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import <PDFNet/PDFViewCtrl.h>


@class ThumbnailSliderViewController;

/**
 * The methods declared by the ThumbnailSliderViewDelegate protocol allow the adopting delegate to respond to messages from
 * the ThumbnailSliderView class.
 *
 */
@protocol ThumbnailSliderViewDelegate <NSObject>
- (void)thumbnailSliderViewInUse:(ThumbnailSliderViewController*)thumbnailSliderViewController;
- (void)thumbnailSliderViewNotInUse:(ThumbnailSliderViewController*)thumbnailSliderViewController;
@end

/**
 * The ThumbnailSliderViewController uses PDFViewCtrl's GetThumbAsync API to show thumbnails
 * of the current page as the slider is moved. See the Complete Reader sample project for example usage.
 *
 */
@interface ThumbnailSliderViewController : UIViewController

/**
 * Returns a new instance of an ThumbnailSliderViewController.
 *
 * @param pdfViewCtrl - An instnce of pdfViewCtrl.
 *
 */
-(id)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl;

/**
 * Sets the slider value programmatically.
 *
 * @param pageNumber - The new page number to set the slider to.
 *
 */
-(void)setSliderValue:(int)pageNumber;

/**
 * Used to set a thumbnail for a given page, to called by the delegate of PTPDFViewCtrl.
 *
 * @param image - The thumbnail image.
 *
 * @param pageNum - the page number of the thumbnail.
 *
 */
-(void)setThumbnail:(UIImage*)image forPage:(int)pageNum;

/**
 * An object that conforms to the ThumbnailSliderViewDelegate protocol.
 *
 */
@property (nonatomic, assign) id<ThumbnailSliderViewDelegate> delegate;


@end
