//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

#import <PDFNet/PDFViewCtrl.h>


@class OutlineViewController;

/**
 * The methods declared by the OutlineViewControllerDelegate protocol allow the adopting delegate to respond to messages from
 * the AnnotationViewController class.
 *
 */
@protocol OutlineViewControllerDelegate <NSObject>
@optional
- (void)outlineViewController: (OutlineViewController*)OutlineViewController selectedBookmark: (NSDictionary*)aBookmark;
- (void)outlineViewControllerDidCancel: (OutlineViewController*)OutlineViewController;
- (void)outlineViewControllerSwitchToAnnotations:(OutlineViewController*)OutlineViewController;
- (void)outlineViewControllerSwitchToBookmarks:(OutlineViewController*)OutlineViewController;
@end


/**
 * The AnnotationViewController will display a docuemnt's outline (bookmakrs) that can be used to
 * navigate the document in PTPDFViewCtrl. See the sample project Complete Reader for
 * example usage.
 *
 */
@interface OutlineViewController : UITableViewController

/**
 * An object that conforms to the OutlineViewControllerDelegate protocol.
 *
 */
@property (nonatomic, assign) id<OutlineViewControllerDelegate> delegate;

/**
 * If true, will display a toggle button that can be used to replace the control with an AnnotaitonViewController.
 */
@property (nonatomic, assign) BOOL alsoAnnotations;

/**
 * If true, will display a toggle button that can be used to replace the control with an BookmarkViewController.
 */
@property (nonatomic, assign) BOOL alsoBookmarks;


/**
 * Returns a new instance of an OutlineViewController.
 *
 */
- (id)initWithPDFViewCtrl: (PTPDFViewCtrl*)pdfViewCtrl;


/**
 * Refresh the contents of the OutlineViewController
 */
- (void)refresh;


@end
