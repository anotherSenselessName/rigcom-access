//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

#import <PDFNet/PDFViewCtrl.h>

UIImage *AnnotationImage(int type);

@class AnnotationViewController;

/**
 * The methods declared by the AnnotationViewControllerDelegate protocol allow the adopting delegate to respond to messages from
 * the AnnotationViewController class.
 *
 */
@protocol AnnotationViewControllerDelegate <NSObject>
@optional
- (void)annotationViewController: (AnnotationViewController*)annotationViewController selectedAnnotaion: (NSDictionary*)anAnnotation;
- (void)annotationViewControllerDidCancel: (AnnotationViewController*)annotationViewController;
- (void)annotationViewControllerSwitchToBookmarks: (AnnotationViewController*)annotationViewController;
- (void)annotationViewControllerSwitchToOutline: (AnnotationViewController*)annotationViewController;
@end


/**
 * The AnnotationViewController will display a list of all annotations in a document being viewed by a PTPDFViewCtrl
 * The list will contain any comments that have been added to the annotations, and selecting an annotation
 * will scroll the PTPDFViewCtrl to the position of the annotation. See the sample project Complete Reader for
 * example usage.
 *
 */
@interface AnnotationViewController : UITableViewController

/**
 * An object that conforms to the AnnotationViewControllerDelegate protocol.
 *
 */
@property (nonatomic, assign) id<AnnotationViewControllerDelegate> delegate;

/**
 * If true, will display a toggle button that can be used to replace the control with an BookmarkViewController.
 */
@property (nonatomic, assign) BOOL alsoBookmarks;

/**
 * If true, will display a toggle button that can be used to replace the control with an OutlineViewController.
 */
@property (nonatomic, assign) BOOL alsoOutline;

/**
 * Returns a new instance of an AnnotationViewController.
 *
 */
- (id)initWithPDFViewCtrl: (PTPDFViewCtrl*)pdfViewCtrl;

@end
