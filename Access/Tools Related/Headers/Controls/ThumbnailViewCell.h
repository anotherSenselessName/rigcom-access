//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

@interface ThumbnailViewCell : UICollectionViewCell
{
    UILabel *_label;
    UIImage *_image;
    UIImageView *_thumbnailView;
    CAShapeLayer *_shapeLayer;
    UIButton *_checkBox;
    BOOL _isSelected;
}

@property (nonatomic, retain) UIButton *checkbox;
@property (nonatomic, assign) BOOL nightMode;

-(void)setPageNumber:(NSInteger)pageNumber isCurrentPage:(BOOL)isCurrent isEditing:(BOOL)isEditing isChecked:(BOOL)isChecked;
-(void)setThumbnail:(UIImage*)image forPage:(NSInteger)pageNum;

@end
