//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "CreateToolBase.h"

@class FreeHandCreate;

@protocol FreeHandCreateDelegate <NSObject>

@optional

/**
 *
 * Selector called when a user adds a new stroke by touching the screen (as opposed to a redo action)
 */
- (void)strokeAdded:(FreeHandCreate*)freeHandCreate;

@end

@interface FreeHandCreate : CreateToolBase {
    CGContextRef context;
	NSMutableArray* m_redo_strokes;
	NSMutableArray* m_free_hand_strokes;
    NSMutableArray* m_free_hand_points;
}

// following selectors useful if implemting FreeHandCreate with multistrokeMode YES
-(void)undoStroke;
-(void)redoStroke;
-(BOOL)canUndoStroke;
-(BOOL)canRedoStroke;
-(void)commitAnnotation;

@property (nonatomic, assign) BOOL multistrokeMode;
@property (nonatomic, weak) id<FreeHandCreateDelegate> delegate;

@end
