//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class PTPage;
@class PTPDFDoc;

@interface TRNStampManager : NSObject

-(BOOL)HasDefaultSignature;
-(PTPDFDoc*)GetDefaultSignature;
-(void)DeleteDefaultSignatureFile;
-(PTPDFDoc*)CreateSignature:(NSMutableArray*)points withColor:(UIColor*)strokeColor withinRect:(CGRect)rect makeDefault:(BOOL)asDefault;


@end
