//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface NoFadeTiledLayer : CATiledLayer {
    
}

@end
