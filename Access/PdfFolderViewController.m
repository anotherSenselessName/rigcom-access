//
//  PdfFolderViewController.m
//  Access
//
//  Created by Sobharaj Mohapatra on 17/04/16.
//  Copyright © 2016 Priya Kaushik. All rights reserved.
//

#import "PdfFolderViewController.h"
#import "PdfListTableViewCell.h"
#import "PdfDetailViewController.h"

@interface PdfFolderViewController ()

@end

@implementation PdfFolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!arrTempPdfData)
    {
        arrTempPdfData = [[NSMutableArray alloc]init];
    }

//    restClient = _restClientOlder;
    
    if(_folderArray.count)
    {
    PdfFileBO *pdf =[_folderArray lastObject];
        _lblTitle.text = pdf.strFileName;
    }
    
    if(!arrData)
        arrData =[[NSMutableArray alloc] init];

    BOOL isdownloading = [self downloadNextFolderData:_folderArray];

}

-(BOOL)downloadNextFolderData:(NSMutableArray *)array
{
    @synchronized(self)
    {
        if (!arrFolders) {
            arrFolders = [[NSMutableArray alloc]init];
        }
        if (array) {
            [arrFolders addObjectsFromArray:array];
        }
        if (arrFolders.count) {
            
            [self getFilesForFolder:[[arrFolders objectAtIndex:0] serverFileId] andHash:[[arrFolders objectAtIndex:0] hashDB]]; //++
            [arrFolders removeObjectAtIndex:0];
            return YES;
        }
    }
    return NO;
}
-(void)getFilesForFolder:(NSString *)folderId andHash:(NSString *)hash
{
    if(hash == nil)
    {
        hash = @"";
    }
    NSArray *arr = [[NSArray alloc] initWithObjects:folderId,hash, nil];
    [self performSelectorOnMainThread:@selector(getFolderDataWithDetails:) withObject:arr waitUntilDone:YES];
}
-(void)getFolderDataWithDetails:(NSArray *)arr
{
    if (![self restClient]) {
        return;
    }
    //    if (!isLoggedOut)
    {
        NSString *folderId = [arr objectAtIndex:0];
        NSString *hash = [arr objectAtIndex:1];
        [[self restClient] loadMetadata:folderId withHash:hash];
    }
    
    
}
- (DBRestClient*)restClient {
    if (restClient == nil) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (void)restClient:(DBRestClient*)client loadedMetadata:(DBMetadata*)metadata
{
    [self getAllTheMetaData:metadata];
}

-(void)getAllTheMetaData:(DBMetadata *)metadata
{
    if([NSThread mainThread] == [NSThread currentThread])
    {
        [self performSelectorInBackground:@selector(getAllTheMetaData:) withObject:metadata];
        return;
    }
    
    
    photosHash = nil;
    photosHash = metadata.hash ;
    
    NSArray* validExtensions = [NSArray arrayWithObjects:@"pdf", nil];
    NSMutableArray* newPhotoPaths = [NSMutableArray new];
    NSMutableArray *directoryArray = [[NSMutableArray alloc]init];
    NSMutableArray *modifiedArray = [[NSMutableArray alloc]init];
    
    [arrData removeAllObjects];
    
    for (DBMetadata* child in metadata.contents)
    {
        NSString* extension = [[child.path pathExtension] lowercaseString];
        
        [newPhotoPaths addObject:child.path];
        
        if ( [validExtensions indexOfObject:extension] != NSNotFound)
        {
            PdfFileBO *pdfFileBO = [[PdfFileBO alloc] init];
            pdfFileBO.strFileName = child.filename;
            pdfFileBO.fileSize = child.totalBytes;
            pdfFileBO.filePath = child.path;
            pdfFileBO.serverFileId = child.path;
            pdfFileBO.isfile = YES;
            [modifiedArray addObject:pdfFileBO];
            
            pdfFileBO = nil;
            
        }
        if(child.isDirectory)
        {
            PdfFileBO *pdfFileBO = [[PdfFileBO alloc] init];
            pdfFileBO.serverFileId = child.path;
            pdfFileBO.strFileName = child.filename;
            pdfFileBO.isfile = NO;
            [directoryArray addObject:pdfFileBO];
            //            [modifiedArray addObject:pdfFileBO];
            
            NSLog(@"child.filename:%@",child.filename);
            pdfFileBO = nil;
            
        }
        
    }
    
    if(modifiedArray.count)
    {
        [arrTempPdfData addObjectsFromArray:modifiedArray];
    }
    
    NSMutableArray *arrDataTemp = [[NSMutableArray alloc] init];
    [arrDataTemp removeAllObjects];
    
    
    [arrData removeAllObjects];
    
    NSMutableArray *arrtemp = nil;
    
    NSInteger tagHH = 0;
    
    for (int i = 0; i<arrTempPdfData.count; i++)
    {
        PdfFileBO *pdfFileBO = [arrTempPdfData objectAtIndex:i];
        
        
        if(arrtemp==nil)
            arrtemp = [[NSMutableArray alloc] init];
        
        pdfFileBO.tag = [NSString stringWithFormat:@"%d",1000+i];
        
        tagHH = 1000+i;
        
        [arrtemp addObject:pdfFileBO];
        if(arrtemp.count==3)
        {
            [arrData addObject:arrtemp];
            arrtemp = nil;
        }
        
        [arrDataTemp addObject:pdfFileBO];
        
        pdfFileBO = nil;
    }
    
    if(arrtemp.count)
        [arrData addObject:arrtemp];
    
    arrTotalData = [[NSMutableArray alloc]init];
    
    [arrTotalData addObjectsFromArray:arrTempPdfData];

    
    for (PdfFileBO *folder in directoryArray)
    {
        tagHH++;
        
        folder.tag = [NSString stringWithFormat:@"%d",tagHH];
        NSMutableArray *arrFol = [[NSMutableArray alloc] initWithObjects:folder, nil];
        
        [arrDataTemp addObject:folder];
        
        [arrTotalData addObject:folder];

        [arrData addObject:arrFol];
        
    }
    

    
    
    [self performSelectorOnMainThread:@selector(reloadDataInTblView) withObject:nil waitUntilDone:YES];
    
    
    // BOOL isdownloading = [self downloadNextFolderData:directoryArray];
    //    if(!isdownloading)
    //    {
    // All Data completed
    //    }
    
}

-(void)reloadDataInTblView
{
    
    [_tableView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 30;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *viewHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-5, 30)];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.font =[UIFont fontWithName:@"HelveticaNeue" size:16];
    [viewHeader addSubview:lblTitle];
    
    if(section==0)
    {
        lblTitle.text = @"Completed Form";
        
    }
    else
    {
        lblTitle.text = @"Selected Form";
        
    }
    
    viewHeader.backgroundColor = [UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0];
    
    return viewHeader;
    
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(section==0)
    {
        return arrSavedPdfs.count;
        
    }
    else
    {
        
        return arrData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0)
    {
        
        static NSString *cellid=@"CompletedPdfListTableViewCell";
        
        PdfListTableViewCell *cell=(PdfListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"PdfListTableViewCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        NSMutableArray *arrtemp = [arrSavedPdfs objectAtIndex:indexPath.row];
        
        
        if(arrtemp.count ==1)
        {
            cell.imgPdf1.hidden = NO;
            cell.btnTitle1.hidden = NO;
            PdfFileBO *pdfFileBO = [arrtemp objectAtIndex:0];
            [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
            cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
            
            
            [cell.btnTitle1 addTarget:self action:@selector(SavedpdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnTitle2.hidden = YES;
            cell.imgPdf2.hidden = YES;
            cell.imgPdf3.hidden = YES;
            cell.btnTitle3.hidden = YES;
        }
        else if(arrtemp.count ==2)
        {
            cell.imgPdf1.hidden = NO;
            cell.btnTitle1.hidden = NO;
            PdfFileBO *pdfFileBO = [arrtemp objectAtIndex:0];
            [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
            cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
            [cell.btnTitle1 addTarget:self action:@selector(SavedpdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnTitle2.hidden = NO;
            cell.imgPdf2.hidden = NO;
            PdfFileBO *pdfFileBO1 = [arrtemp objectAtIndex:1];
            [cell.btnTitle2 setTitle:pdfFileBO1.strFileName forState:UIControlStateNormal];
            cell.btnTitle2.tag = [pdfFileBO1.tag integerValue];
            [cell.btnTitle2 addTarget:self action:@selector(SavedpdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.imgPdf3.hidden = YES;
            cell.btnTitle3.hidden = YES;
        }
        
        else if(arrtemp.count ==3)
        {
            cell.imgPdf1.hidden = NO;
            cell.btnTitle1.hidden = NO;
            PdfFileBO *pdfFileBO = [arrtemp objectAtIndex:0];
            [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
            cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
            [cell.btnTitle1 addTarget:self action:@selector(SavedpdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnTitle2.hidden = NO;
            cell.imgPdf2.hidden = NO;
            PdfFileBO *pdfFileBO1 = [arrtemp objectAtIndex:1];
            [cell.btnTitle2 setTitle:pdfFileBO1.strFileName forState:UIControlStateNormal];
            cell.btnTitle2.tag = [pdfFileBO1.tag integerValue];
            [cell.btnTitle2 addTarget:self action:@selector(SavedpdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.imgPdf3.hidden = NO;
            cell.btnTitle3.hidden = NO;
            PdfFileBO *pdfFileBO2 = [arrtemp objectAtIndex:2];
            [cell.btnTitle3 setTitle:pdfFileBO2.strFileName forState:UIControlStateNormal];
            cell.btnTitle3.tag = [pdfFileBO2.tag integerValue];
            [cell.btnTitle3 addTarget:self action:@selector(SavedpdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    
    else
    {
        
        static NSString *cellid=@"PdfListTableViewCell";
        
        PdfListTableViewCell *cell=(PdfListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"PdfListTableViewCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        NSMutableArray *arrtemp = [arrData objectAtIndex:indexPath.row];
        
        cell.imgPdf1.image = [UIImage imageNamed:@"pdf2_iPhone.png"];
        
        
        //pdf2_iPhone.png
        //imgFolder.png
        if(arrtemp.count ==1)
        {
            PdfFileBO *pdfFileBO = [arrtemp objectAtIndex:0];
            
            if(pdfFileBO.isfile == YES)
            {
                cell.imgPdf1.hidden = NO;
                cell.btnTitle1.hidden = NO;
                [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
                cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
                
                
                [cell.btnTitle1 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnTitle2.hidden = YES;
                cell.imgPdf2.hidden = YES;
                cell.imgPdf3.hidden = YES;
                cell.btnTitle3.hidden = YES;
            }
            else
            {
                cell.imgPdf1.image = [UIImage imageNamed:@"imgFolder.png"];
                
                cell.imgPdf1.hidden = NO;
                cell.btnTitle1.hidden = NO;
                [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
                cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
                
                
                [cell.btnTitle1 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnTitle2.hidden = YES;
                cell.imgPdf2.hidden = YES;
                cell.imgPdf3.hidden = YES;
                cell.btnTitle3.hidden = YES;
                
            }
        }
        else if(arrtemp.count ==2)
        {
            cell.imgPdf1.hidden = NO;
            cell.btnTitle1.hidden = NO;
            PdfFileBO *pdfFileBO = [arrtemp objectAtIndex:0];
            [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
            cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
            [cell.btnTitle1 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnTitle2.hidden = NO;
            cell.imgPdf2.hidden = NO;
            PdfFileBO *pdfFileBO1 = [arrtemp objectAtIndex:1];
            [cell.btnTitle2 setTitle:pdfFileBO1.strFileName forState:UIControlStateNormal];
            cell.btnTitle2.tag = [pdfFileBO1.tag integerValue];
            [cell.btnTitle2 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.imgPdf3.hidden = YES;
            cell.btnTitle3.hidden = YES;
        }
        
        else if(arrtemp.count ==3)
        {
            cell.imgPdf1.hidden = NO;
            cell.btnTitle1.hidden = NO;
            PdfFileBO *pdfFileBO = [arrtemp objectAtIndex:0];
            [cell.btnTitle1 setTitle:pdfFileBO.strFileName forState:UIControlStateNormal];
            cell.btnTitle1.tag = [pdfFileBO.tag integerValue];
            [cell.btnTitle1 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnTitle2.hidden = NO;
            cell.imgPdf2.hidden = NO;
            PdfFileBO *pdfFileBO1 = [arrtemp objectAtIndex:1];
            [cell.btnTitle2 setTitle:pdfFileBO1.strFileName forState:UIControlStateNormal];
            cell.btnTitle2.tag = [pdfFileBO1.tag integerValue];
            [cell.btnTitle2 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.imgPdf3.hidden = NO;
            cell.btnTitle3.hidden = NO;
            PdfFileBO *pdfFileBO2 = [arrtemp objectAtIndex:2];
            [cell.btnTitle3 setTitle:pdfFileBO2.strFileName forState:UIControlStateNormal];
            cell.btnTitle3.tag = [pdfFileBO2.tag integerValue];
            [cell.btnTitle3 addTarget:self action:@selector(pdfBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
        
    }
    return nil;
}


-(void)pdfBtnClicked:(UIButton *)btnPdf
{
    NSString *strTag = [NSString stringWithFormat:@"%ld",(long)btnPdf.tag];
    
    NSString *strFormat = [NSString stringWithFormat:@"tag contains '%@'",strTag];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:strFormat];
    NSArray *arr = [arrTotalData filteredArrayUsingPredicate:predicate];
    
    PdfFileBO *fileBO = [arr lastObject];
    
    if(fileBO.isfile)
    {
        UIStoryboard *mainStory = [UIStoryboard storyboardWithName:@"Main"
                                                            bundle: nil];
        PdfDetailViewController *PdfDetailViewController = [mainStory   instantiateViewControllerWithIdentifier:@"PdfDetailViewController"];
        PdfDetailViewController.pdfFileBO = [arr lastObject];
        [self.navigationController pushViewController:PdfDetailViewController animated:YES];
    }
    else
    {
        UIStoryboard *mainStory = [UIStoryboard storyboardWithName:@"Main"
                                                            bundle: nil];
        PdfFolderViewController *PdfDetail = [mainStory   instantiateViewControllerWithIdentifier:@"PdfFolderViewController"];
        PdfDetail.folderArray = (NSMutableArray *)arr;
        //        PdfDetail.restClientOlder = self.restClient;
        [self.navigationController pushViewController:PdfDetail animated:YES];
        
    }
    
}

-(void)SavedpdfBtnClicked:(UIButton *)btnPdf
{
    NSString *strTag = [NSString stringWithFormat:@"%ld",(long)btnPdf.tag];
    
    NSString *strFormat = [NSString stringWithFormat:@"tag contains '%@'",strTag];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:strFormat];
    NSArray *arr = [arrSavedPdfs filteredArrayUsingPredicate:predicate];
    
    
    UIStoryboard *mainStory = [UIStoryboard storyboardWithName:@"Main"
                                                        bundle: nil];
    PdfDetailViewController *PdfDetailViewController = [mainStory   instantiateViewControllerWithIdentifier:@"PdfDetailViewController"];
    PdfDetailViewController.pdfFileBO = [arr lastObject];
    [self.navigationController pushViewController:PdfDetailViewController animated:YES];
    
}

@end
