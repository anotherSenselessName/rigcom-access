//
//  JobMapViewController.m
//  Access
//
//  Created by Sobharaj Mohapatra on 21/11/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import "JobMapViewController.h"
#import "ZSAnnotation.h"
#import "ZSPinAnnotation.h"

@interface JobMapViewController ()

@end

@implementation JobMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self startLocationManager];
    
    [self.jobMapView setShowsUserLocation:YES];
    
   // [self showMapView];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)showMapView
{
    [_jobMapView removeAnnotations:_jobMapView.annotations];
    CLLocationCoordinate2D location;
    location.latitude=37.785834;
    location.longitude=-122.406417;
    
    
    ZSAnnotation *annotation = nil;
    annotation = [[ZSAnnotation alloc] init];
    annotation.coordinate = location;
    
    annotation.title = @"Hi";
        annotation.subtitle = @"Hello";
    
    annotation.type = ZSPinAnnotationTypeTagStroke;
    [_jobMapView addAnnotation:annotation];
    
    
    [_jobMapView setRegion:MKCoordinateRegionMake(location, MKCoordinateSpanMake(0.010f, 0.010f)) animated:YES];
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    
    
    // Don't mess with user location
    if(![annotation isKindOfClass:[ZSAnnotation class]])
        return nil;
    
    //    ZSAnnotation *a = (ZSAnnotation *)annotation;
    static NSString *defaultPinID = @"StandardIdentifier";
    
    // Create the ZSPinAnnotation object and reuse it
    ZSPinAnnotation *pinView = (ZSPinAnnotation *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil){
        pinView = [[ZSPinAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    
    // Set the type of pin to draw and the color
    pinView.annotationType = ZSPinAnnotationTypeTag
    ;
    pinView.canShowCallout = YES;
    
    
    pinView.annotationColor = [UIColor redColor];
    //    pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return pinView;
    
}

-(void)startLocationManager
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    _locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest; // 100 m
    [_locationManager startUpdatingLocation];
    
    
}
// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    _locationManager.delegate = nil;
    CLLocation *location = [locations lastObject];
    userCutterntlocation=location.coordinate;
    
    
    [self centerTheMapwithCurrentLoc];
}


-(void)centerTheMapwithCurrentLoc
{
    if(userCutterntlocation.latitude)
        [self.jobMapView setRegion:MKCoordinateRegionMake(userCutterntlocation, MKCoordinateSpanMake(0.02f, 0.02f)) animated:YES];
    
}


- (IBAction)btnCloseClicked:(id)sender {
    
    if(self.callBack != nil && [self.callBack respondsToSelector:@selector(dismissPopover)])
    {
        [self.callBack dismissPopover];
    }
}
@end
