//
//  PdfFolderViewController.h
//  Access
//
//  Created by Sobharaj Mohapatra on 17/04/16.
//  Copyright © 2016 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PdfFileBO.h"

@interface PdfFolderViewController : UIViewController<DBRestClientDelegate>

{
    NSMutableArray *arrData;
    NSMutableArray *arrTotalData;

    NSMutableArray *arrSavedPdfs;
    NSMutableArray *arrFolders;
    NSMutableArray *arrTempPdfData;
    NSArray* photoPaths;
    NSString* photosHash;
    DBRestClient* restClient;
    NSString* currentPhotoPath;

}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic , strong)DBRestClient *restClientOlder;

@property (nonatomic , strong)NSMutableArray *folderArray;
- (IBAction)btnBackClicked:(id)sender;

@end
