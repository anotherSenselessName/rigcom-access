//
//  LoginViewController.m
//  
//
//  Created by Priya Kaushik on 09/09/15.
//
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.passwordTextField.text =@"";
    self.emailTextField.text =@"";
    
    
}

- (IBAction)login:(UIButton *)sender {
    
    
    [_emailTextField resignFirstResponder];
    [_passwordTextField  resignFirstResponder];
    
 //   [self performSegueWithIdentifier:@"enableLoginSegue" sender:self];
   
    /*
    
    if(![self isValidEmail:self.emailTextField.text])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning !" message:@"Invalid email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        alert =  nil;
        
        return;
    }
     
    */
    
    if(self.passwordTextField.text.length == 0 || self.emailTextField.text == 0 )
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning !" message:@"Please enter the all the fields." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        alert =  nil;
        
        return;
    }
    
       [APP_DELEGATE showCustomLoader:self];
    
    [self performSelectorInBackground:@selector(LoginWithDeatils) withObject:nil];
    
}

#pragma mark -
#pragma mark - ValidateEmail Methods
#pragma mark -

-(BOOL)isValidEmail: (NSString *) strEmail1
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:strEmail1];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    _verticalLayoutvWlogIn.constant = -100;
    
    [UIView commitAnimations];

    return YES;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    _verticalLayoutvWlogIn.constant = -100;
    
    [UIView commitAnimations];
    

    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    _verticalLayoutvWlogIn.constant = 0;
    [UIView commitAnimations];

    //[textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [textField resignFirstResponder];
    return YES;
}


#pragma mark- Login parser Delegate
-(void)LoginWithDeatils
{
    LoginParser *parser = [[LoginParser alloc]init];
    parser.delegate =  self;
    [parser LoginWithEmail:self.emailTextField.text password:self.passwordTextField.text];
    parser =  nil;
    
}


-(void)LoginSuccessWithDeatils:(NSDictionary *)dictDetails
{
    [APP_DELEGATE removeCustomLoader:self];
    [self performSelectorOnMainThread:@selector(LoginSuccessWithDeatilsOnMainthread:) withObject:dictDetails waitUntilDone:NO];
}

-(void)LoginFailedWithError:(NSError *)error
{
        [APP_DELEGATE removeCustomLoader:self];

    [self performSelectorOnMainThread:@selector(LoginFailedWithErrorOnMainthread:) withObject:error waitUntilDone:NO];
}

-(void)LoginSuccessWithDeatilsOnMainthread:(NSDictionary *)dictDetails
{
    
    NSString *strStatus = [NSString stringWithFormat:@"%@",[dictDetails objectForKey:@"Status"]];
    
    
    if([strStatus isEqualToString:@"1"])
    {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"ShowHome"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        
        
         APP_DELEGATE.LoggedUserId = [NSString stringWithFormat:@"%@",[dictDetails objectForKey:@"userId"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:APP_DELEGATE.LoggedUserId forKey:@"userId"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSegueWithIdentifier:@"enableLoginSegue" sender:self];
        
    }
    else
    {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"ShowHome"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning !" message:@"No record found with this combination. Please re-try" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        alert =  nil;
        
    }
    
    
}

-(void)LoginFailedWithErrorOnMainthread:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning !" message:@"some error occured . Please try after some time" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
    alert =  nil;
    
    
}


@end
