//
//  PopOverTableViewController.h
//  Access
//
//  Created by Priya Kaushik on 29/09/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedObject <NSObject>

-(void)selectedItem:(NSString *)item;

@end
@interface PopOverTableViewController : UITableViewController
@property (nonatomic,strong) NSArray *contentArray;
@property (nonatomic,strong) id<SelectedObject>delegate;
@end
