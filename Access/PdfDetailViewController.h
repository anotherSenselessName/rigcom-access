//
//  PdfDetailViewController.h
//  Access
//
//  Created by Sobharaj Mohapatra on 29/10/15.
//  Copyright © 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PdfFileBO.h"
#import <PDFNet/PDFNetOBJC.h>
#import <PDFNet/PDFViewCtrl.h>
#import "Tool.h"
#import "PanTool.h"
#import "ToolManager.h"

@class DBRestClient;
@interface PdfDetailViewController : UIViewController<DBRestClientDelegate>
{
    DBRestClient* restClient;
    PTPDFViewCtrl* pdfViewCtrl;
    NSString *strRev;
    UIDeviceOrientation currentOrientation;
}
@property (nonatomic, strong) PdfFileBO *pdfFileBO;
@property (nonatomic, readonly) DBRestClient* restClient;
@property (nonatomic, strong) PTPDFDoc *document;

@property (nonatomic,assign)BOOL IsFromCompletedForms;


- (IBAction)btnBackClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)btnSaveClicked:(id)sender;

@end
