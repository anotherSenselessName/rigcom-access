//
//  AppDelegate.h
//  Access
//
//  Created by Priya Kaushik on 09/09/15.
//  Copyright (c) 2015 Priya Kaushik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import <DropboxSDK/DropboxSDK.h>
#import "CustomLoader.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,DBSessionDelegate, DBNetworkRequestDelegate>
{
    NSString *relinkUserId;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) NSString *relinkUserId;
@property (nonatomic,strong) NSMutableArray *arrPdfFile;
@property (nonatomic,strong) NSMutableArray *arrJOBS;

-(void)showCustomLoader:(UIViewController *)senderController;
-(void)removeCustomLoader:(UIViewController *)senderController;

-(void)showCustomLoaderInWidow:(UIWindow *)senderController;
-(void)removeCustomLoaderInWidow:(UIWindow *)senderController;
@property (strong, nonatomic)NSString *LoggedUserId;

@end

