//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2014 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#ifndef   H_CALLBACK
#define   H_CALLBACK

#ifdef SWIG

#include <C/Common/TRN_Types.h>
#include <Common/BasicTypes.h>
#include <PDF/Selection.h>

namespace pdftron { 
	namespace PDF {

/**
 *	SWIG director's base class
 *
 *	Contains virtual functions which match the signature of the PDFNet function pointers.
 *  Each virtual function has a matching static function calling it.
 *	User extends this class in the target language and overrides the function of interest.
 *	The static function is assigned to the function pointer.
 */
class Callback
{
public:
	Callback() {}
	virtual ~Callback() {
	}

// PDFView Callbacks
	virtual void RenderBeginEventProc(){}

	static void StaticRenderBeginEventProc(void* data)
	{
		Callback* cb = (Callback*) data;
		cb->RenderBeginEventProc();
	}

	virtual void RenderFinishEventProc(bool cancelled){}

	static void StaticRenderFinishEventProc(void* data, bool cancelled)
	{
		Callback* cb = (Callback*) data;
		cb->RenderFinishEventProc(cancelled);
	}

	virtual void ErrorReportProc (const char* message) {}

	static void StaticErrorReportProc (const char* message, void* data)
	{
		Callback* cb = (Callback*) data;
		cb->ErrorReportProc(message);
	}

	virtual void CurrentPageProc(int current_page, int num_pages) {}

	static void StaticCurrentPageProc(int current_page, int num_pages, void* data)
	{
		Callback* cb = (Callback*) data;
		cb->CurrentPageProc(current_page, num_pages);
	}

	virtual void CurrentZoomProc(double curr_zoom_proc) {}

	static void StaticCurrentZoomProc(double curr_zoom_proc, void* data)
	{
		Callback* cb = (Callback*) data;
		cb->CurrentZoomProc(curr_zoom_proc);
	}

	virtual void ThumbAsyncHandler(int page_num, bool was_thumb_found, const char* thumb_buf, int thumb_width, int thumb_height) {}
	
	static void StaticThumbAsyncHandler(int page_num, bool was_thumb_found, const char* thumb_buf, int thumb_width, int thumb_height, void* custom_data)
	{
		Callback* cb = (Callback*) custom_data;
		cb->ThumbAsyncHandler(page_num, was_thumb_found, thumb_buf, thumb_width, thumb_height);
	}

	virtual void RequestRenderInWorkerThread() {}

	static void StaticRequestRenderInWorkerThread(void* custom_data)
	{
		Callback* cb = (Callback*) custom_data;
		cb->RequestRenderInWorkerThread();
	}

	virtual void FindTextHandler(bool success, PDF::Selection selection) {}

	static void StaticFindTextHandler(bool success, PDF::Selection selection, void* custom_data)
	{
		Callback* cb = (Callback*) custom_data;
		cb->FindTextHandler(success, selection);
	}
	
	virtual void CreateTileProc(
    	char* buffer, int originX, int originY, int width, int height,
       	int pagNum, long long cellNumber,
       	bool finalRender, bool predictionRender,
       	int tilesRemaining, bool firstTile,
       	int canvasWidth, int canvasHeight, int cellSideLength, int cellPerRow, int cellPerCol,
       	int thumbnailId
   	) { }
	
	static void StaticCreateTileProc(             
       	void* customData,
    	char* buffer, int originX, int originY, int width, int height,
       	int pageNum, long long cellNumber,
       	bool finalRender, bool predictionRender,
       	int tilesRemaining, bool firstTile,
       	int canvasWidth, int canvasHeight, int cellSideLength, int cellPerRow, int cellPerCol,
       	int thumbnailId
	)
	{
		Callback* cb = (Callback*) customData;
		cb->CreateTileProc(
			buffer, originX, originY, width, height,
			pageNum, cellNumber,
			finalRender, predictionRender,
			tilesRemaining, firstTile,
			canvasWidth, canvasHeight, cellSideLength, cellPerRow, cellPerCol,
			thumbnailId
		);
	}
	
	virtual void RemoveTileProc(int canvasNumber, Int64 cellNumber, int thumbnailId) { }
	
	static void StaticRemoveTileProc(void* customData, int canvasNumber, Int64 cellNumber, int thumbnailId)
	{
		Callback* cb = (Callback*) customData;
		cb->RemoveTileProc(canvasNumber, cellNumber, thumbnailId);
	}

	virtual void PartDownloadedProc(int dlType, TRN_PDFDoc doc, unsigned int pageNum, unsigned int objNum, const char* message) { }

	static void StaticPartDownloadedProc(int dlType, TRN_PDFDoc doc, unsigned int pageNum, unsigned int objNum, const char* message, void* customData)
	{
		Callback* cb = (Callback*) customData;
		cb->PartDownloadedProc(dlType, doc, pageNum, objNum, message);
	}
};

	};	// namespace PDF
};	// namespace pdftron

#endif  // SWIG
#endif  // H_CALLBACK
